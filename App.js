import React from 'react';
import { Image } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createBottomTabNavigator } from "react-navigation-tabs";
import { wp } from './src/UtilMethods/Utils';
import DrawerNav from './src/components/DrawerNav';
import { fromBottom, fromRight } from './src/transition/Transition';

// SCREEN BEFORE AUTH
import Splash from './src/screens/authStack/Splash';
import Auth from './src/screens/authStack/Auth';
import SignIn from './src/screens/authStack/SignIn';
import SignUp from './src/screens/authStack/SignUp';
import OnBoarding from './src/screens/authStack/OnBoarding';

// HomeStack
import Budget from './src/screens/HomeStack/Budget';
import SuperMarket from './src/screens/HomeStack/SuperMarket';
import Store from './src/screens/HomeStack/Store';
import RepeatPurchase from './src/screens/HomeStack/RepeatPurchase';
import DeliverySchedule from './src/screens/HomeStack/DeliverySchedule';
import Settings from './src/screens/HomeStack/Settings';
import VerTodoList from './src/screens/HomeStack/VerTodoList';
import MyCartItem from './src/screens/HomeStack/MyCartItem';
import CardDetail from './src/components/CardDetail';
import Details from './src/screens/authStack/Details';
import SelectAddress from './src/screens/authStack/SelectAddress';
import AddAddress from './src/screens/authStack/AddAddress';
import Comment from './src/screens/authStack/Comment';
import Cupone from './src/screens/authStack/Cupone';
import StoreTiming from './src/screens/HomeStack/StoreTiming';
import Payment from './src/screens/HomeStack/Payment';
import Paypal from './src/screens/HomeStack/Paypal';
import Stripe from './src/screens/HomeStack/Stripe';
import Map from './src/screens/HomeStack/Map';

console.disableYellowBox = true;

const handleCustomTransition = ({scenes}) => {

  const nextScene = scenes[scenes.length - 1];

  if (nextScene && nextScene.route.routeName === 'CardDetail' || nextScene.route.routeName === 'CardDetail' ) {
    return fromBottom(400);
  }
  return fromRight(400);
};

const TabNavigator = createBottomTabNavigator(
    {
      Store: {
        screen: Store,
        navigationOptions: {
          tabBarLabel:"Store",
          tabBarIcon: ({ tintColor }) => (
              <Image source={require('./src/assets/icon/store.png')}
                     style={{width: wp('12'), height: wp('12'), tintColor: tintColor}} />
          )
        },
      },
      SuperMarket: {
        screen: SuperMarket,
        navigationOptions: {
          tabBarLabel:"Store",
          tabBarIcon: ({ tintColor }) => (
              <Image source={require('./src/assets/icon/superMarket.png')}
                     style={{width: wp('12'), height: wp('12'), tintColor: tintColor}} />
          )
        },
      },
      RepeatPurchase: {
        screen: RepeatPurchase,
        navigationOptions: {
          tabBarLabel:"Store",
          tabBarIcon: ({ tintColor }) => (
              <Image source={require('./src/assets/icon/repeatPurchase.png')}
                     style={{width: wp('12'), height: wp('12'), tintColor: tintColor}} />
          )
        },
      },
      DeliverySchedule: {
        screen: DeliverySchedule,
        navigationOptions: {
          tabBarLabel:"Store",
          tabBarIcon: ({ tintColor }) => (
              <Image source={require('./src/assets/icon/deliverySchedule.png')}
                     style={{width: wp('12'), height: wp('12'), tintColor: tintColor}} />
          )
        },
      },
      Settings: {
        screen: Settings,
        navigationOptions: {
          tabBarLabel:"Store",
          tabBarIcon: ({ tintColor }) => (
              <Image source={require('./src/assets/icon/Settings.png')}
                     style={{width: wp('12'), height: wp('12'), tintColor: tintColor}} />
          )
        },
      }
    },
    {
      tabBarOptions: {
        showLabel: false,
        activeTintColor: "#FF6F00",
        inactiveTintColor: "#000000",
        style: {
          // backgroundColor: "#002580",
          height: 70
        }
      }
    }
);

const ProfileNavigator = createDrawerNavigator({
  Drawer: TabNavigator
}, {
  initialRouteName: 'Drawer',
  contentComponent: DrawerNav,
  drawerWidth: 300,
  drawerBackgroundColor: '#6c9f27'

});

const AppNavigator = createStackNavigator({
  Splash: {
    screen: Splash
  },
  Auth: {
    screen: Auth
  },
  SignIn: {
    screen: SignIn
  },
  SignUp: {
    screen: SignUp
  },
  OnBoard: {
    screen: OnBoarding
  },
  Budget: {
    screen: Budget
  },
  VerTodoList: {
    screen: VerTodoList
  },
  MyCartItem: {
    screen: MyCartItem
  },
  CardDetail: {
    screen: CardDetail
  },
  Detail: {
    screen: Details
  },
  SelectAddress: {
    screen: SelectAddress
  },
  AddAddress: {
    screen: AddAddress
  },
  Comment: {
    screen: Comment
  },
  Cupone: {
    screen: Cupone
  },
  StoreTiming: {
    screen: StoreTiming
  },
  Payment: {
    screen: Payment
  },
  Paypal: {
    screen: Paypal
  },
  Map: {
    screen: Map
  },
  Stripe: {
    screen: Stripe
  },
  TabNavigator: {
    screen: ProfileNavigator
  },
}, {
  headerMode: 'none',
  transitionConfig: (nav) => handleCustomTransition(nav),
});

export default class AppWrapper extends React.Component {
  render() {
    return (
        <AppContainer />
    );
  }
}

const AppContainer = createAppContainer(AppNavigator);
