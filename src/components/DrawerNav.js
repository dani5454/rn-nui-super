import React from 'react';
import {Image, TouchableOpacity,SafeAreaView, Text, View, StyleSheet} from 'react-native';
import {wp, hp} from "../UtilMethods/Utils";

export default class DrawerNav extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            dataSource: ['Lácteos', 'Carnes y mariscos', 'Embutidos', 'Frutas y verduras', 'Jugos', 'Enlatados', 'Harinas']
        }
    }


    render() {
        return(
            <SafeAreaView style={{flex: 1}}>
                <View style={{alignItems: 'center'}}>
                    <Image source={require('./../assets/images/notification_logo.png')}
                           style={{width: wp('15%'),height: wp('15%') }}
                           />
                </View>
                <View style={{marginLeft: wp('3%'), marginRight: wp('3%'), flex: 1}}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <View style={{flexDirection: 'column'}}>
                            <Text style={styles.colors}>Marvin Dertage</Text>
                            <Text style={styles.colors}>commo@gmail.com</Text>
                            <Text style={styles.colors}>9999-9999</Text>
                        </View>
                        <View style={{flexDirection: 'column', alignItems: 'center'}}>
                            <View style={{borderRadius: 50, height: 45, width: 45, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center'}}>
                                <Text>MD</Text>
                            </View>
                            <Text style={styles.colors}>Cuenta</Text>
                        </View>
                    </View>
                    <View style={{borderWidth: 2, borderColor: 'white'}}/>
                    <View style={{flex: 1, marginTop: hp('4%')}}>
                        {this.state.dataSource.map((item, index) => {
                            return(
                                <TouchableOpacity key={index} style={{marginTop: hp('3%')}}>
                                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                        <View style={{backgroundColor: 'white',width: 30, height: 30, borderRadius: 50}}/>
                                        <Text style={[styles.colors, {marginLeft: wp('4%')}]}>{item}</Text>
                                    </View>
                                </TouchableOpacity>
                            );
                        })}
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
   colors: {
       color: 'white'
   }
});
