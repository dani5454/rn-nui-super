import React, { Component } from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';

import { FlatList, RectButton } from 'react-native-gesture-handler';

import AppleStyleSwipeableRow from './AppleStyleSwipeableRow';
import {hp, wp} from "../UtilMethods/Utils";

let that = '';

const Row = ({ item }) => (
    <RectButton style={styles.rectButton} onPress={() => console.log('nista')}>
        <View style={styles.cardContainer}>
            <View>
                <Image source={{uri: item.image}}
                       style={{resizeMode: 'contain',height: wp('14%'),width:wp('14%')}}
                />
            </View>
            <View style={{flexDirection: 'column',width: wp('40%')}}>
                <Text>
                    {item.description}
                </Text>
                <Text style={{opacity: .4, fontSize: 12}}>
                    {item.name}
                </Text>
            </View>
            <View>
                <Text style={{opacity: .4, fontSize: 14}}>x{item.quanity}</Text>
            </View>
            <View>
                <Text>{item.price} = L.{item.price * item.quanity}</Text>
            </View>
        </View>
    </RectButton>
);

const onClick = (item, click) => {
    that.props.onPress(item, click);
};

const SwipeableRow = ({ item, index }) => {
    return (
        <AppleStyleSwipeableRow>
            <Row item={item} onPress={onClick} />
        </AppleStyleSwipeableRow>
    );
};

export default class SwipeGesture extends Component {

    constructor(props) {
        super(props);

        this.state={
            totalPrice:0,
            didUpdate: this.props.didUpdate
        };

        that = this;
    }

    componentDidMount() {
        let sum = 0;
        this.props.card.forEach((value, index) =>{
            if(value.shop === this.props.marketName) {
                sum += parseInt(value.price) * value.quanity;
            }
        });

        this.setState({totalPrice: sum});
    }

    UNSAFE_componentWillReceiveProps(nextProps) {
        let count = 0, sum = 0;
        if(count === 0) {
            nextProps.card.forEach((value, index) => {
                if(value.shop === this.props.marketName) {
                    sum += parseInt(value.price) * value.quanity;
                }
            });
            count = count + 1;
        }

        this.setState({totalPrice: sum});
    }

    render() {
        return (
            <View style={{flex:1}}>
                <View>
                    <View style={{
                        height: hp('8%'), justifyContent: 'space-between',
                        alignItems: 'center',backgroundColor: '#6c9f27', flexDirection: 'row'
                    }}>
                        <Text style={{fontSize: 18,color: 'white', left: wp('5%')}}>{this.props.marketName}</Text>
                        <Text style={{fontSize: 18,color: 'white', right: wp('5%')}}>L. {this.state.totalPrice}</Text>
                    </View>
                </View>
                <FlatList
                    data={this.props.card}
                    ItemSeparatorComponent={() => <View style={styles.separator} />}
                    renderItem={({ item, index }) => (
                        <SwipeableRow item={item} index={index} />
                    )}
                    keyExtractor={(item, index) => `message ${index}`}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    rectButton: {
        flex: 1,
        height: 80,
        paddingVertical: 10,
        paddingHorizontal: 20,
        justifyContent: 'space-between',
        flexDirection: 'column',
        backgroundColor: 'white',
    },
    separator: {
        backgroundColor: 'rgb(200, 199, 204)',
        height: StyleSheet.hairlineWidth,
    },
    cardContainer: {
        flexDirection: 'row',justifyContent: 'space-between',
        alignItems: 'center', height: hp('6%')
    }
});
