import React from 'react';
import {ActivityIndicator, Text, Modal, View} from "react-native";

class AppLoading {

    renderLoading(visible) {
        return (<View>{visible ?
                <Modal transparent={true} onRequestClose={() => null} visible={visible}>
                    <View style={{flex: 1, backgroundColor: '#00000070', alignItems: 'center', justifyContent: 'center'}}>
                        <View style={{borderRadius: 15, backgroundColor: "#fff", padding: 25}}>
                            <ActivityIndicator size="large" color='#030031'/>
                            <Text style={{fontSize: 16, color: '#D64B12',opacity:1}}>Cargando...</Text>
                        </View>
                    </View>
                </Modal> : null}</View>

        );
    }
}

export default new AppLoading();
