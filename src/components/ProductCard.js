import React from "react";
import {Image, StyleSheet, Text, TouchableOpacity,TouchableWithoutFeedback,ActivityIndicator, View} from "react-native";
import {hp, wp} from "../UtilMethods/Utils";

export default class ProductCard extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            addItem: this.props.item.disabled,
            count: this.props.item.count,
            disabled: false,
            onDecrement: false
        };
    }

    _incrementProduct = (item) => {
        if(!this.state.disabled) {
            this.setState({disabled: true,onDecrement: true,count: this.state.count + 1}, () => {
                setTimeout(() => {
                    this.setState({disabled: false,onDecrement: false});
                }, 1100);
                this.props.addToCart(this.state.count, item);
            });
        }
    };

    _decrementProduct = (item) => {
        // item.quanity = this.props.item.count;
        let { count } = this.state;
        if(!this.state.onDecrement) {
            if (count === 1) {
                this.setState({onDecrement: true,disabled: true,addItem: !this.state.addItem,count: this.state.count - 1}, () => {
                    setTimeout(() => {
                        this.setState({onDecrement: false,disabled: false});
                    }, 1100);
                    this.props.decrementProduct(this.state.count - 1, item)
                });
            } else {
                this.setState({onDecrement: true, disabled: true,count: this.state.count - 1}, () => {
                    setTimeout(() => {
                        this.setState({onDecrement: false,disabled: false});
                    }, 1100);
                    this.props.decrementProduct(this.state.count, item)
                });
            }
        }
    };

    _onCardPress = (item) => {
        this.props.onCardPress(item)
    };

    render() {
        const {item, index} = this.props;
        return (
            <View style={[styles.productCard, {marginLeft: index > 0 ? wp('5%') : null}]}>
                <TouchableWithoutFeedback
                    style={{flex: 1}}
                    onPress={() => this._onCardPress(item)}
                >
                    <View style={{flex: 1}}>
                        <View style={{alignItems: 'center'}}>
                            <Image source={{uri: item.image}}
                                   style={{
                                       resizeMode: 'cover',
                                       width: wp('20%'),
                                       height: wp('20%'),
                                   }}
                            />
                        </View>
                        <View style={{flexDirection: 'column', marginTop: wp('2%')}}>
                            <Text style={{fontSize: 11, opacity: .5, marginLeft: 5}}>{item.name}</Text>
                            <Text style={{
                                fontSize: 11,
                                fontWeight: 'bold',
                                marginLeft: 5
                            }}>{item.description}</Text>
                            <Text style={{
                                fontSize: 11,
                                fontWeight: 'bold',
                                marginLeft: 5,
                                color: 'green'
                            }}>L.{' '}{item.price}</Text>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
                {!this.state.addItem ?
                    <TouchableOpacity
                        onPress={() => this.setState({addItem: true})}
                        style={{
                            marginTop: wp('2%'), height: hp('4.5%'),
                            backgroundColor: 'grey',
                            borderBottomRightRadius: wp('5.5%'),
                            borderBottomLeftRadius: wp('5.5%'),
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                        <Text style={{fontSize: 12, color: 'white'}}>Agregar</Text>
                    </TouchableOpacity> :
                    <View style={{
                        justifyContent: 'space-between',
                        marginTop: wp('2%'),
                        alignItems: 'center',
                        flexDirection: 'row',
                        height: hp('4.5%'),
                    }}>
                        <TouchableOpacity
                            onPress={() => this._decrementProduct(item)}
                            disabled={this.state.disabled}
                            style={{marginLeft: wp('3%')}}>
                            <Image source={require('./../assets/icon/ic_minus.png')}
                                   style={{
                                       resizeMode: 'cover',
                                       width: wp('6%'),
                                       height: wp('6%'),
                                       tintColor: '#D64B12'
                                   }}
                            />
                        </TouchableOpacity>
                        <View>
                            {this.state.disabled && this.state.onDecrement ? <ActivityIndicator /> :
                                <Text style={{fontSize: 16}}>{this.state.count}</Text>
                            }
                        </View>
                        <TouchableOpacity
                            onPress={() => this._incrementProduct(item)}
                            disabled={this.state.onDecrement}
                            style={{marginRight: wp('3%')}}>
                            <Image source={require('./../assets/icon/ic_add.png')}
                                   style={{
                                       resizeMode: 'cover',
                                       width: wp('5%'),
                                       height: wp('5%'),
                                       tintColor: '#D64B12'
                                   }}
                            />
                        </TouchableOpacity>
                    </View>
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    productCard: {
        backgroundColor: 'white',
        width: wp('35%'),
        flexDirection: 'column',
        borderBottomRightRadius: wp('5%'),
        borderBottomLeftRadius: wp('5%')
    }
});
