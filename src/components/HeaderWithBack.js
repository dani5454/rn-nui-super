import React from 'react';
import {Image, TouchableOpacity,Text, View, StyleSheet} from "react-native";
import { wp, hp } from './../UtilMethods/Utils';

export default HeaderWithBack = (props) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.headerProfile} onPress={props.onLeftIcon}>
                <Image resizeMode="contain" style={{tintColor: 'white',height: wp('6%'),width:wp('6%')}}
                       source={require('./../assets/icon/ic_back.png')}/>
                {props.show && (
                       <Text style={{fontSize: 18, color: '#ffffff'}}>Corrito</Text>
                )}
            </TouchableOpacity>
            <View>
                <Text style={{fontSize: 22, color: 'white'}}>
                    {props.text}
                </Text>
            </View>
            {props.rightIcon === true ?
                <TouchableOpacity style={styles.headerMenu} onPress={props.onRightIcon}>
                    <Image resizeMode="contain" style={{tintColor: 'white', height: wp('7%'), width: wp('7%')}}
                           source={props.icon}/>
                </TouchableOpacity> :
                <View/>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height:hp('8%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#D64B12'
    },
    headerProfile: {
        paddingLeft: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    headerMenu: {
        paddingRight: 10,
    }
});
