import React from 'react';
import {TouchableOpacity,Text, View, Image, StyleSheet} from "react-native";
import { wp, hp } from './../UtilMethods/Utils';

export default AddAddressHeader = (props) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.headerProfile} onPress={props.onLeftIcon}>
                {props.isImage ?
                    <Image
                        source={require('./../assets/icon/ic_back.png')}
                        resizeMode="contain"
                        style={{tintColor: 'white',height: wp('6%'),width:wp('6%')}}
                    />
                    :
                    <Text style={{color: '#ffffff', fontSize: 18, opacity: .6}}>Cancelar</Text>
                }
            </TouchableOpacity>
            <View>
                <Text style={{fontSize: 22, color: 'white'}}>
                    {props.text}
                </Text>
            </View>
            {props.isRight ?
                <TouchableOpacity style={styles.headerMenu} onPress={props.onRightIcon}>
                    <Text style={{color: '#ffffff', fontSize: 18, opacity: .6}}>{props.rightText}</Text>
                </TouchableOpacity>
                :
                <View/>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height:hp('8%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#D64B12'
    },
    headerProfile: {
        paddingLeft: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    headerMenu: {
        paddingRight: 10,
    }
});
