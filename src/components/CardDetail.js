import React from 'react';
import {Image, Text, TouchableOpacity, View, SafeAreaView} from 'react-native';
import CommonDataManager from '../service/Singlenton';
import {hp, wp} from './../UtilMethods/Utils';
import service from '../firebase/Firebase';

export default class CardDetail extends React.Component {
    instance = CommonDataManager.getInstance();

    constructor(props) {
        super(props);

        this.state = {
            card: this.props.navigation.state.params.card,
            cartCount: 0,
            marketName: this.props.navigation.state.params.marketName,
        };

        console.log('CARD--->>>', this.props.navigation.state.params.card);
    }

    componentDidMount() {
        let userCart = this.instance.getUserCart();
        if(userCart.length > 0) {
            let cartCount = 0;
            userCart.forEach((value) =>{
                cartCount += parseInt(value.quanity);
            });
            this.setState({cartCount: cartCount});
        }
    }

    _onLeftIconPress = () => {
        this.props.navigation.goBack();
    };

    _onCartPress = () => {
        let userCart = this.instance.getUserCart();
        this.props.navigation.navigate('MyCartItem', {marketName: this.state.marketName, myCartItem: userCart});
    };

    render() {
        const { card } = this.state;
        return(
            <View style={{flex: 1}}>
                <SafeAreaView style={{flex: 1}}>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                        <TouchableOpacity
                            onPress={() => this._onLeftIconPress()}
                        >
                            <Image resizeMode="contain" source={require('./../assets/icon/ic_cross.png')}
                                   style={{left: wp('4%'),height: wp('6%'),width:wp('6%')}}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this._onCartPress()}
                        >
                            {this.state.cartCount > 0 && (
                                <View style={{
                                    borderRadius: 100,
                                    width: '42%',
                                    left: '10%',
                                    backgroundColor: 'white',
                                    alignItems: 'center',
                                    top: wp('4%'),
                                    zIndex: 1
                                }}>
                                    <Text>{this.state.cartCount}</Text>
                                </View>
                            )}
                            <Image resizeMode="contain" source={require('./../assets/icon/cart.png')}
                                   style={{opacity: .6,bottom: this.state.cartCount > 0?'5%':null,right: wp('4%'),
                                       tintColor: '#D64B12',height: wp('10%'),width:wp('10%')}}
                            />
                        </TouchableOpacity>
                    </View>
                    <View style={{flex: 1}}>
                        <View style={{alignItems:'center', marginTop: hp('5%')}}>
                            <Image resizeMode="contain" source={{uri: card.image}}
                                   style={{height: wp('50%'),width:wp('50%')}}
                            />
                            <View style={{alignItems: 'center'}}>
                                <Text style={{fontSize: 32}}>{card.description}</Text>
                                <Text style={{fontSize: 18,marginTop:hp('1%'),opacity: .5}}>{card.name}</Text>
                                <Text style={{fontSize: 18,marginTop:hp('1%'),color: 'green'}}>L.{' '}{card.price}</Text>
                            </View>
                        </View>
                        <View style={{flex: 1,justifyContent: 'flex-end', alignItems: 'center'}}>
                            <View style={{
                                width: wp('50%'),
                                justifyContent: 'space-between',
                                alignItems: 'center',
                                flexDirection: 'row',
                                height: hp('4.5%'),
                            }}>
                                <TouchableOpacity
                                    onPress={() => this._decrementProduct(card)}
                                    style={{marginLeft: wp('3%')}}>
                                    <Image source={require('./../assets/icon/ic_minus.png')}
                                           style={{
                                               resizeMode: 'cover',
                                               width: wp('6%'),
                                               height: wp('6%'),
                                               tintColor: '#D64B12'
                                           }}
                                    />
                                </TouchableOpacity>
                                <View>
                                    <Text style={{fontSize: 16}}>{card.count}</Text>
                                </View>
                                <TouchableOpacity
                                    onPress={() => this._incrementProduct(card)}
                                    style={{marginRight: wp('3%')}}>
                                    <Image source={require('./../assets/icon/ic_add.png')}
                                           style={{
                                               resizeMode: 'cover',
                                               width: wp('5%'),
                                               height: wp('5%'),
                                               tintColor: '#D64B12'
                                           }}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </SafeAreaView>
            </View>
        )
    }

    _decrementProduct = async (card) => {
        let arr = [];let newArray = [];
        let oneItem = false;
        let userCart = this.instance.getUserCart();

        if (card.count > 0) {
            userCart.map(async (cart, index) => {
                if (card.name === cart.name && card.category_type === cart.category_type && cart.shop === this.state.marketName) {
                    if (cart.quanity === 1) {
                        userCart.splice(index, 1);
                        cart.count = 0;
                        this.setState({cart});
                        oneItem = true;
                    } else {
                        let count = cart.quanity - 1;
                        card.quanity = count;
                        card.count = count;
                        arr.push(card);
                        userCart.splice(index, 1);
                    }
                }
            });

            if (oneItem) {
                newArray = [...userCart];
            } else {
                newArray = [...arr, ...userCart];
            }

            this.instance.setUserCart(newArray);
            this.setState({myCartItem: newArray});
            await service._addToCart('123', newArray, (resp) => {
                if (!resp.error) {
                    this.setState({ cartCount: this.state.cartCount - 1});
                }
            });
        }
    };

    _incrementProduct = async (card) => {
        let arr = [];let newArray = [];
        let userCart = this.instance.getUserCart();
        userCart.map(async (cart, index) => {
            if(card.name === cart.name && card.category_type === cart.category_type && cart.shop === this.state.marketName) {
                let count = cart.quanity + 1;
                card.quanity = count;
                card.count = count;
                this.setState({card});
                arr.push(card);
                userCart.splice(index,1);
            }
        });
        if(arr.length === 0) {
            let count = card.count + 1;
            card.quanity = count;
            card.count = count;
            this.setState({card});
            arr.push(card);
        }
        card.shop = this.state.marketName;
        newArray = [...arr, ...userCart];
        this.instance.setUserCart(newArray);
        this.setState({myCartItem: newArray});
        await service._addToCart('123',newArray, (resp) => {
            if(!resp.error) {
                this.setState({ cartCount: this.state.cartCount + 1});
            }
        });
    }

}
