import React from 'react';
import {Image,Text, TouchableOpacity,Platform, View, StyleSheet, ScrollView, FlatList} from "react-native";
import { wp, hp } from './../UtilMethods/Utils';
import ProductCard from "./ProductCard";

export default class ProductList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            addItem: false,
            productList: this.props.productName,
            showProducts: false
        };
    }

    componentDidMount() {
        this.props.productList.map((item) => {
            let pro = this.props.userCartList.find(find =>
                find.name === item.name && find.category_type === item.category_type && find.shop === this.props.shop);
            if (pro) {
                item.count = pro.quanity;
                item.disabled = true;
            } else {
                item.count = 0;
                item.disabled = false;
            }
        });
        this.setState({productList: this.props.productList, showProducts: true});
    }

    render() {
        const {props} = this;
        return (
            <View style={[styles.container, {backgroundColor: props.color}]}>
                <View style={styles.more}>
                    <Text style={{fontSize: 18, marginLeft: wp('3%')}}>{props.productList[0].category_type}</Text>
                    <TouchableOpacity
                        onPress={() => props.onVerTodo(props.productList, props.catName)}
                        style={{
                            flexDirection: 'row', alignItems: 'center',
                            borderWidth: .3, justifyContent: 'center',
                            marginRight: wp('5%'), width: wp('25%'),
                            height: hp(Platform.OS==='ios'?'4%':'5%'), borderRadius: wp('2%')
                        }}>
                        <Text style={{opacity: .5}}>Ver todo</Text>
                        <Image source={require('./../assets/icon/next.png')}
                               style={{width: wp('4%'), height: wp('4%'), opacity: .5}}
                        />
                    </TouchableOpacity>
                </View>
                <View style={[styles.productContainer]}>
                    <ScrollView
                        horizontal
                        showsHorizontalScrollIndicator={false}
                    >
                        {this.state.showProducts && (
                            <FlatList
                                data={this.state.productList}
                                horizontal
                                renderItem={({item, index}) => {
                                    return (
                                        <ProductCard item={item}
                                                     index={index}
                                                     addToCart={props.addToCart}
                                                     onCardPress={props.onCardPress}
                                                     decrementProduct={props.decrementProduct}
                                        />
                                    );
                                }}
                                contentContainerStyle={{
                                    paddingBottom: 10
                                }}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        )}

                    </ScrollView>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: wp('3%')
    },
    more: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    productContainer: {
        marginTop: hp('4%'),
        flexDirection: 'row',
        marginLeft: wp('3%')
    },
    productCard: {
        backgroundColor: 'white',
        width: wp('35%'),
        flexDirection: 'column',
        borderBottomRightRadius: wp('5%'),
        borderBottomLeftRadius: wp('5%')
    }
});
