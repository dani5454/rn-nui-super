import React,{Component} from  'react';
import {StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';
import { hp, wp } from '../UtilMethods/Utils';

export default class Address extends Component {
    constructor(props) {
        super(props);

        this.state = {

        };
    }

    render(){
        const { userAddress } = this.props;
        return (
            <View style={styles.container}>
                <Image source={require('../assets/icon/ic_home-512.png')} style={{width:wp('6%') ,height:wp('6%')}} />
                <View style={{flexDirection: 'column', flex: 1, marginLeft: wp('6%')}}>
                    <Text style={{fontWeight:"300", fontSize: 18}}>{userAddress.alias}</Text>
                    <Text style={{opacity: .3}}>{userAddress.addressAndNumber}</Text>
                </View>
                <TouchableOpacity
                    onPress={() => this.props.onCheckPress(userAddress)}
                    style={{right: wp('3%')}}>
                    <Image source={require('../assets/icon/ic_check-512.png')}
                           style={{tintColor: userAddress.check?'#79B45D':'#000000',width:wp('5%') ,height:wp('5%')}} />
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container:{
        flexDirection: 'row',
        height: hp('6%'),
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomWidth: .4,
        borderBottomColor: 'grey'
    },

});
