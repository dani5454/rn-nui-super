import React, { Component } from 'react';
import { Animated, StyleSheet, View, I18nManager, Image } from 'react-native';

import { RectButton } from 'react-native-gesture-handler';

import Swipeable from 'react-native-gesture-handler/Swipeable';

export default class AppleStyleSwipeableRow extends Component {
    // renderLeftActions = (progress, dragX) => {
    //     const trans = dragX.interpolate({
    //         inputRange: [0, 50, 100, 101],
    //         outputRange: [-20, 0, 0, 1],
    //     });
    //     return (
    //         <RectButton style={styles.leftAction} onPress={this.close}>
    //             <Animated.Text
    //                 style={[
    //                     styles.actionText,
    //                     {
    //                         transform: [{ translateX: trans }],
    //                     },
    //                 ]}>
    //                 Archive
    //             </Animated.Text>
    //         </RectButton>
    //     );
    // };
    renderRightAction = (imageSource, click, color, x, progress) => {
        const trans = progress.interpolate({
            inputRange: [0, 1],
            outputRange: [x, 0],
        });
        const pressHandler = () => {
            this.close();
            this.props.children.props.onPress(this.props.children.props.item, click)
        };
        return (
            <Animated.View style={{ flex: 1, transform: [{ translateX: trans }] }}>
                <RectButton
                    style={[styles.rightAction, { backgroundColor: color }]}
                    onPress={pressHandler}>
                    <Image source={imageSource} resizeMethod={'contain'}
                           style={{width: 25, height: 25, tintColor: 'white'}}
                           />
                </RectButton>
            </Animated.View>
        );
    };
    renderRightActions = progress => (
        <View style={{ width: 192, flexDirection: I18nManager.isRTL? 'row-reverse' : 'row' }}>
            {this.renderRightAction(require('./../assets/icon/ic_minus.png'), 'minus', '#C8C7CD', 256, progress)}
            {this.renderRightAction(require('./../assets/icon/ic_add.png'), 'add', '#C8C7CD', 192, progress)}
            {this.renderRightAction(require('./../assets/icon/ic_edit.png'), 'edit', '#ffab00', 128, progress)}
            {this.renderRightAction(require('./../assets/icon/ic_delete.png'), 'delete','#dd2c00', 64, progress)}
        </View>
    );
    updateRef = ref => {
        this._swipeableRow = ref;
    };
    close = () => {
        this._swipeableRow.close();
    };
    render() {
        const { children } = this.props;
        return (
            <Swipeable
                ref={this.updateRef}
                friction={2}
                leftThreshold={30}
                rightThreshold={40}
                // renderLeftActions={this.renderLeftActions}
                renderRightActions={this.renderRightActions}>
                {children}
            </Swipeable>
        );
    }
}

const styles = StyleSheet.create({
    leftAction: {
        flex: 1,
        backgroundColor: '#497AFC',
        justifyContent: 'center',
    },
    actionText: {
        color: 'white',
        fontSize: 16,
        backgroundColor: 'transparent',
        padding: 10,
    },
    rightAction: {
        alignItems: 'center',
        flex: 1,
        justifyContent: 'center',
    },
});
