import React from 'react';
import {Image, TouchableOpacity, View,Text, StyleSheet,TextInput, Platform} from "react-native";
import { wp, hp } from './../UtilMethods/Utils';

export default class HomeHeader extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            addItem: false,
        }
    }
    render() {
        const { props } = this;
        return (
            <View style={styles.container}>
                <View style={styles.headerProfile} />
                <View style={{
                    alignItems: 'center',
                    flexDirection: 'row',
                    backgroundColor: 'pink',
                    borderRadius: 10,
                    opacity: .5,
                    height: hp('6%'),
                    width: wp('60%')
                }}>
                    <Image resizeMode="contain"
                           style={{marginLeft: wp('3%'), tintColor: 'white', height: wp('7%'), width: wp('7%')}}
                           source={require('./../assets/icon/ic_search.png')}/>
                    <View style={{flex: 1, marginLeft: wp('3%')}}>
                        <TextInput
                            placeholder={'Buscar producto'}
                            autoCorrect={false}
                            placeholderTextColor="#ffffff"
                            onChangeText={password => props.onChangeText(password)}
                        />
                    </View>
                </View>
                <TouchableOpacity style={styles.headerMenu} onPress={props.onRightIcon}>
                    {props.onShowItem && (
                        <View style={{
                            height: '25%', borderRadius: 100,
                            width: '40%', alignSelf: 'flex-end', alignItems: 'center',
                            justifyContent: 'center', top: '19%', right: '5%', zIndex: 1,
                            backgroundColor: 'white'
                        }}>
                            <Text>{props.cartItem}</Text>
                        </View>
                    )}
                    <Image resizeMode="contain"
                           style={{bottom: props.onShowItem ?'10%':null, opacity: .6,
                               tintColor: 'white', height: wp('12%'),
                               width: wp('12%')}}
                           source={require('./../assets/icon/cart.png')}/>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        height:Platform.OS==='ios'?hp('10%'):hp('9%'),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#D64B12'
    },
    headerProfile: {
        paddingLeft: 10,
    },
    headerMenu: {
        paddingRight: 10,
    }
});
