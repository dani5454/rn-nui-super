import React from 'react';
import {Image, TouchableOpacity, View,SafeAreaView, StyleSheet, Platform} from "react-native";
import { wp, hp } from './../UtilMethods/Utils';

export default Header = (props) => {
    return (
        <SafeAreaView style={styles.container}>
            <TouchableOpacity style={styles.headerProfile} onPress={props.onLeftIcon}>
                <Image resizeMode="contain" style={{height: wp('12%'),width:wp('12%')}}
                       source={require('./../assets/icon/location.png')}/>
            </TouchableOpacity>
            <View>
                <Image resizeMode="contain" style={{height: wp('15%'),width:wp('15%')}}
                       source={require('./../assets/images/logo.png')} />
            </View>
            <TouchableOpacity style={styles.headerMenu} onPress={props.onRightIcon}>
                <Image resizeMode="contain" style={{height: wp('12%'),width:wp('12%')}}
                       source={require('./../assets/icon/dropDown.png')}/>
            </TouchableOpacity>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        height:Platform.OS==='ios'?hp('10%'):hp('9%'),
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    headerProfile: {
        paddingLeft: 10,
    },
    headerMenu: {
        paddingRight: 10,
    }
});
