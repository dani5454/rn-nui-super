import React from 'react';
import {Image,Text, View,TouchableOpacity, StyleSheet} from "react-native";
import { wp, hp } from './../UtilMethods/Utils';

export default Selector = (props) => {
    return (
        <View style={props.viewStyle}>
            <Text style={{paddingLeft: wp('5%'),opacity: .3, fontSize: 16}}>{props.title}</Text>
            <View style={styles.separator}/>
            <TouchableOpacity
                onPress={props.onSelect}
                style={{flexDirection: 'row',alignItems: 'center',height: hp('6%'),paddingLeft: wp('5%')}}>
                <Image source={props.icon}
                       style={{resizeMode: 'contain',height: wp('5%'), width: wp('5%'), tintColor: '#D64B12'}}
                />
                <Text style={{marginLeft: wp('4%')}}>{props.selectorText}</Text>
                {props.isRightContent && (
                    <View style={{flex:1,alignItems: 'flex-end'}}>
                        <Text style={{right: wp('16%'), top: wp('3%')}}>{props.rightContentText}</Text>
                        <Image
                            source={require('./../assets/icon/ic_right-512.png')}
                            style={{resizeMode: 'contain',bottom:wp('2%'),height: wp('6%'), width: wp('6%'), tintColor: 'grey'}}
                        />
                    </View>
                )}
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height:hp('8%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#D64B12'
    },
    headerProfile: {
        paddingLeft: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    headerMenu: {
        paddingRight: 10,
    }
});
