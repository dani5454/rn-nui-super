import React from 'react';
import {View, TextInput, StyleSheet} from 'react-native';
import {hp, wp} from '../UtilMethods/Utils';

export default InputContainer = (props) => {
    return (
        <View style={[styles.inputContainer, props.inputStyle]}>
            <TextInput
                style={{paddingLeft: wp('3%')}}
                placeholder={props.placeholder}
                value={props.value}
                autoCapitalize='none'
                onChangeText={props.onChangeText}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    inputContainer: {
        height: hp('3%'),
        borderBottomWidth: .6,
        borderBottomColor: 'grey',
    }
});
