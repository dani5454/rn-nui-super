import firebase from 'react-native-firebase';

class Firebase {
    _signIn = (email, password, callback) => {
        return firebase
            .auth()
            .signInWithEmailAndPassword(email, password)
            .then(currentUser => {
                return callback({error: false, response: currentUser.user});
            })
            .catch(error => {
                return callback({error: true, errorMessage: error.message});
            });
    };

    _signUp = (email, password, callback) => {
        return firebase
            .auth()
            .createUserWithEmailAndPassword(email, password)
            .then(currentUser => {
                return callback({error: false, response: currentUser.user});
            })
            .catch(error => {
                return callback({error: true, errorMessage: error.message});
            });
    };

    _signUpWithCredentials = (credentials, callback) => {
        return firebase
            .auth()
            .signInWithCredential(credentials)
            .then(currentUser => {
                return callback({error: false, response: currentUser});
            })
            .catch(error => {
                return callback({error: true, errorMessage: error.message});
            });
    };

    _setUpUserFbProfile = (uid, currentUser) => {
        return firebase
            .firestore()
            .collection("Profile")
            .doc(uid)
            .set({
                email: currentUser.profile.email,
                name: currentUser.profile.first_name,
                lastName: currentUser.profile.last_name,
                isFacebook: true,
                isGoogle: false,
                timeSchedule: '',
                comments: '',
                facebookId: currentUser.profile.id,
                socialProfilePic: currentUser.profile.picture.data.url,
                phone: '',
                address: '',
                customer: true,
                driver: false,
            });
    };

    _setUpUserGoogleProfile = (uid, currentUser) => {
        return firebase
            .firestore()
            .collection("Profile")
            .doc(uid)
            .set({
                email: currentUser.profile.email,
                name: currentUser.profile.given_name,
                lastName: currentUser.profile.family_name,
                isFacebook: false,
                isGoogle: true,
                timeSchedule: '',
                comments: '',
                facebookId: '',
                socialProfilePic: currentUser.profile.picture,
                phone: '',
                address: '',
                customer: true,
                driver: false,
            });
    };

    _setUpUserProfile = (uid, email, name, lastName, phone, address) => {
        return firebase
            .firestore()
            .collection("Profile")
            .doc(uid)
            .set({
                email: email,
                name: name,
                lastName: lastName,
                isFacebook: false,
                isGoogle: false,
                facebookId: '',
                timeSchedule: '',
                comments: '',
                socialProfilePic: '',
                phone: phone,
                address: address,
                customer: true,
                driver: false,
            });
    };

    // _getProductList = (productName, callback) => {
    //     return firebase
    //         .firestore()
    //         .collection("Products")
    //         .doc(productName)
    //         .get()
    //         .then(snapshot => {
    //             return callback({error: false, data:snapshot.data()});
    //         }).catch((error) => {
    //             return callback({error: true, data:'Internal Server Error,\nPlease try again'});
    //         });
    // };

    _getProductList = (productName, callback) => {
        return firebase
            .firestore()
            .collection("ProductList")
            .where('shop','==',productName)
            .get()
            .then(snapshot => {
                return callback({error: false, data:snapshot});
            }).catch((error) => {
                return callback({error: true, data:'Internal Server Error,\nPlease try again'});
            });
    };

    fetchMarkets = (callback) => {
        return firebase
            .firestore()
            .collection("ProductList")
            .get()
            .then(snapshot => {
                return callback({error: false, data:snapshot});
            }).catch((error) => {
                return callback({error: true, data:'Internal Server Error,\nPlease try again'});
            });
    };

    _getUserProfile = (uid, callback) => {
        return firebase
            .firestore()
            .collection("Profile")
            .doc(uid)
            .get()
            .then(snapshot => {
                return callback({error: false, data:snapshot.data()});
            }).catch((error) => {
                return callback({error: true, data:'Internal Server Error,\nPlease try again'});
            });
    };

    // _uploadProducts = (productName, productList) => {
    //     return firebase
    //         .firestore()
    //         .collection("Products")
    //         .doc(productName)
    //         .set({
    //             productList: productList
    //         });
    // };

    // uploadImage(imagePath, superMarketName, imageName) {
    //     const image = imagePath.uri;
    //     const Blob = RNFetchBlob.polyfill.Blob;
    //     const fs = RNFetchBlob.fs;
    //     window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
    //     window.Blob = Blob;
    //
    //     let uploadBlob = null;
    //     const imageRef = firebase.storage().ref(`productsImage/${superMarketName}/${imageName}.png`);
    //     let mime = "image/jpg";
    //     return fs.readFile(imagePath, "base64")
    //         .then(data => {
    //             console.log('data--->>>', data);
    //             return Blob.build(data, {type: `${mime};BASE64`});
    //         })
    //         .then(blob => {
    //             console.log('blob--->>>', blob);
    //             uploadBlob = blob;
    //             return imageRef.put(blob._ref, {contentType: mime});
    //         })
    //         .then(() => {
    //             uploadBlob.close();
    //             return imageRef.getDownloadURL();
    //         })
    //         .then(url => {
    //             console.log('url--->>>', url);
    //             return url;
    //         })
    //         .catch(error => {
    //             console.log(error);
    //         });
    // }

    // _getProductsImage = (productCategory,callback) => {
    //   return  firebase.database().ref('productImages/' + productCategory+'/').once('value', (snapshot) => {
    //       return callback({data:snapshot.val()});
    //   });
    // };

    _getUserCartList = (callback) => {
        return firebase
            .firestore()
            .collection("UserCart")
            .doc(firebase.auth().currentUser.uid)
            .get()
            .then(snapshot => {
                return callback({error: false, data:snapshot.data()});
            }).catch((error) => {
                return callback({error: true, data:'Internal Server Error,\nPlease try again'});
            });
    };

    _addToCart = (uid, item, callback) => {
        let currentUser = firebase.auth().currentUser.uid;
        return firebase
            .firestore()
            .collection("UserCart")
            .doc(currentUser)
            .set({
                UserCartArray: item,
            }).then(() => callback({error: false, message: 'Successfully written'}))
            .catch(err => {
                return callback({error: true, message: 'Internal Server Error'})
            })
    };

    _updateUserAddress = (userAddress, callback) => {
        return firebase
            .firestore()
            .collection("Profile")
            .doc(firebase.auth().currentUser.uid)
            .update({
                address: userAddress
            }).then(() => {
                return callback({error: false, message:'Success'});
            }).catch((error) => {
                return callback({error: true, message:error.message});
            });
    };

    _getCupones = (callback) => {
        return firebase
            .firestore()
            .collection("Cupones")
            .doc('ProductsCupones')
            .get()
            .then(snapshot => {
                return callback({error: false, data:snapshot.data()});
            }).catch((error) => {
                return callback({error: true, message:'Internal Server Error,\nPlease try again'});
            });
    };

    _updateComments = (comment, callback) => {
        return firebase
            .firestore()
            .collection("Profile")
            .doc(firebase.auth().currentUser.uid)
            .update({
                comments: comment
            }).then(() => {
                return callback({error: false, message:'Success'});
            }).catch((error) => {
                return callback({error: true, message:error.message});
            });
    };

    _updateTimeSchdule = (timeSchdule, callback) => {
        return firebase
            .firestore()
            .collection("Profile")
            .doc(firebase.auth().currentUser.uid)
            .update({
                timeSchedule: timeSchdule
            }).then(() => {
                return callback({error: false, message:'Success'});
            }).catch((error) => {
                return callback({error: true, message:error.message});
            });
    };

    _removeCart = (callback) => {
        return firebase
            .firestore()
            .collection("UserCart")
            .doc(firebase.auth().currentUser.uid)
            .update({
                UserCartArray: ''
            }).then(() => {
                return callback({error: false, message:'Success'});
            }).catch((error) => {
                return callback({error: true, message:error.message});
            });
    };

    _setUserOrder = (orderID,user, userOrderList, payPalResponse, callback) => {
        return firebase
            .firestore()
            .collection("OrderQueue")
            .doc(firebase.auth().currentUser.uid)
            .set({
                orderId: orderID,
                userDetail: user,
                orderTime: new Date().toLocaleString(),
                userOrder: userOrderList,
                payPalResponse: payPalResponse,
                status: 'requested',
                userUID: firebase.auth().currentUser.uid
            }).then(() => {
                return callback({error: false, message:'Success'});
            }).catch((error) => {
                return callback({error: true, message:error.message});
            });
    };

    _getOrderId = (callback) => {
        return firebase
            .firestore()
            .collection("order_id")
            .doc('orderID')
            .get()
            .then(snapshot => {
                return callback({error: false, data:snapshot.data()});
            }).catch((error) => {
                return callback({error: true, message:'Internal Server Error,\nPlease try again'});
            });
    };

    _updateOrderId = (orderId, callback) => {
        return firebase
            .firestore()
            .collection("order_id")
            .doc('orderID')
            .update({
                orderId: orderId
            }).then(() => {
                return callback({error: false, message:'Success'});
            }).catch((error) => {
                return callback({error: true, message:error.message});
            });
    };

    _signOut = async () => {
        await firebase.auth().signOut().done();
    }
}

const firebaseModal = new Firebase();
export default firebaseModal;
