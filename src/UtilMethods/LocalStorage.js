import AsyncStorage from '@react-native-community/async-storage';

class LocalStorage {

    _StoreData = async (key, value) => {
        const data = JSON.stringify(value);
        try {
            await AsyncStorage.setItem(key, data);
        } catch (error) {
            console.log(error.message);
        }
    };

    _GetStoredData = async (key) => {
        let retrievedData;
        try {
            retrievedData = await AsyncStorage.getItem(key);
        } catch (error) {
            console.log(error.message);
        }
        return JSON.parse(retrievedData);
    };

    _RemoveStoredData = async (key) => {
        try {
            await AsyncStorage.removeItem(key);
        } catch (error) {
            console.log(error.message);
        }
    };
}

export default new LocalStorage();
