import React from 'react';
import { View,Text,SafeAreaView,Image,ImageBackground,TouchableOpacity } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import Swiper from './../../components/Swiper';
import { wp, hp } from './../../UtilMethods/Utils';

export default class OnBoarding extends React.Component {

    _moveToScreenWithStackReset = (screenName) => {
        const resetAction = StackActions.reset({
            index: 0,
            routeName: screenName,
            actions: [NavigationActions.navigate({ routeName: screenName })],
        });
        this.props.navigation.dispatch(resetAction)
    };

    render() {
        return (
            <View style={{flex: 1}}>
                <Swiper showsButtons={true}
                    // buttonWrapperStyle={{marginTop: hp('20%'), right: 0,}}
                >
                    <SafeAreaView style={{flex: 1}}>
                        <ImageBackground
                            source={require('./../../assets/images/Welcome-1.jpg')}
                            resizeMode={'cover'}
                            style={{flex: 1}}
                        >
                            <View style={{flex: 1, flexDirection: 'column'}}>
                                <View style={{flex: 1,marginBottom: 75,alignItems: 'center', justifyContent: 'flex-end'}}>
                                    <View style={{marginBottom: wp('1%')}}>
                                        <Text>Vamos al súper por ti</Text>
                                    </View>
                                    <View style={{alignItems: 'center', flexDirection: 'row'}}>
                                        <Image source={require('./../../assets/images/intro_logo_1.png')}
                                               style={{resizeMode: 'contain',width: wp('20%'), height: wp('20%')}}
                                               />
                                        <Image source={require('./../../assets/images/intro_logo_2.png')}
                                               style={{resizeMode: 'contain',width: wp('25%'), height: wp('25%'), marginLeft: 40}}
                                               />
                                        <Image source={require('./../../assets/images/intro_logo_3.png')}
                                               style={{resizeMode: 'contain',width: wp('20%'), height: wp('20%'), marginLeft: 40}}
                                               />
                                    </View>
                                    <View style={{}}>
                                        <Text>y muchos más</Text>
                                    </View>
                                    <TouchableOpacity
                                        onPress={() => this._moveToScreenWithStackReset('TabNavigator')}
                                        style={{alignSelf: 'flex-end', marginRight: 10}}>
                                        <Text>Omitir</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </ImageBackground>
                    </SafeAreaView>
                    <View style={{flex: 1}}>
                        <ImageBackground
                            source={require('./../../assets/images/Welcome-2.jpg')}
                            resizeMode={'cover'}
                            style={{flex: 1}}
                        >
                            <View style={{alignItems: 'center', justifyContent: 'center'}}>
                                <Image source={require('./../../assets/images/logo.png')}
                                       style={{resizeMode: 'contain',width: wp('18%'), height: wp('18%')}}
                                />
                            </View>
                        </ImageBackground>
                    </View>
                    <View style={{flex: 1}}>
                        <ImageBackground
                            source={require('./../../assets/images/Welcome-3.jpg')}
                            resizeMode={'cover'}
                            style={{flex: 1}}
                        >
                            <View style={{alignItems: 'center', justifyContent: 'center'}}>
                                <Image source={require('./../../assets/images/logo.png')}
                                       style={{resizeMode: 'contain',width: wp('18%'), height: wp('18%')}}
                                />
                            </View>
                        </ImageBackground>
                    </View>
                </Swiper>
            </View>
        );
    }
}
