import React from  'react';
import {SafeAreaView, StyleSheet, ScrollView, FlatList, View, StatusBar, Platform} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import HeaderWithBack from './../../components/HeaderWithBack';
import CommonDataManager from '../../service/Singlenton';
import Address from '../../components/Address';
import Service from './../../firebase/Firebase';
import { wp, hp } from './../../UtilMethods/Utils';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? hp('6%') : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

const MyStatusBar = ({backgroundColor, ...props}) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
        <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
);

export default class SelectAddress extends React.Component {
    instance = CommonDataManager.getInstance();

    constructor(props) {
        super(props);

        this.state = {
            showAddress: false,
            addressArray: []
        };

    }

    _onLeftIconPress = () => {
        this.props.navigation.goBack();
    };

    _onRightIconPress = () => {
      this.props.navigation.navigate('AddAddress');
    };

    _onWillFocus = () => {
        let user = this.instance.getUser();
        if(Array.isArray(user.address)) {
            this.setState({addressArray: user.address, showAddress: true});
        }
    };

    render() {
        const { showAddress, addressArray } = this.state;
        return (
            <View style={styles.container}>
                <MyStatusBar backgroundColor="#D64B12" barStyle="light-content" />
                <SafeAreaView style={styles.container}>

                    <NavigationEvents onWillFocus={payload => this._onWillFocus(payload)} />

                    <HeaderWithBack
                        text={'Mis direcciones'}
                        rightIcon={true}
                        icon={require('./../../assets/icon/ic_add-512.png')}
                        onLeftIcon={this._onLeftIconPress.bind(this)}
                        onRightIcon={this._onRightIconPress.bind(this)}
                    />
                    <ScrollView contentContainerStyle={styles.container}>
                        <View style={{flex:1}}>
                            <View style={styles.addressContainer}>
                                {showAddress && (
                                    <FlatList
                                        data={addressArray}
                                        ItemSeparatorComponent={() => <View style={styles.separator} />}
                                        renderItem={({ item, index }) => (
                                            <Address userAddress={item} onCheckPress={this._onCheckPress.bind(this)} />
                                        )}
                                        keyExtractor={(item, index) => `message ${index}`}
                                    />
                                )}
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>
            </View>
        );
    }

    _onCheckPress = (address) => {
        let user = this.instance.getUser();
        let userTrue = user.address.filter((item) => {
            item.check = item.addressAndNumber === address.addressAndNumber && item.alias === item.alias;
            return item;
        });
        this._updateUserAddress(userTrue, user);
    };

    _updateUserAddress = (userAddress, user) => {
        Service._updateUserAddress(userAddress, (resp) => {
            if(!resp.error) {
                this.setState({ addressArray: userAddress});
                user.address = userAddress;
                this.instance.setUser(user);
            }
        });
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    statusBar: {
        height: STATUSBAR_HEIGHT,
    },
    appBar: {
        backgroundColor:'#79B45D',
        height: APPBAR_HEIGHT,
    },
    content: {
        flex: 1,
        backgroundColor: '#33373B',
    },
    addressContainer:{
        paddingLeft: wp('5%'),
    },
    separator: {
        backgroundColor: 'rgb(200, 199, 204)',
        height: StyleSheet.hairlineWidth,
    },
});
