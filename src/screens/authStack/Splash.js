import React from 'react';
import { ImageBackground } from 'react-native';
import {NavigationActions, StackActions} from "react-navigation";
import firebase from "react-native-firebase";
import CommonDataManager from '../../service/Singlenton';
import service from '../../firebase/Firebase';

export default class Splash extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        let common = CommonDataManager.getInstance();

        if(firebase.auth().currentUser) {
            service._getUserProfile(firebase.auth().currentUser.uid,(user) => {
                if(!user.error) {
                    common.setUser(user.data);
                }
                service._getUserCartList(async (userCart) => {
                    if(typeof userCart.data !== "undefined") {
                        if(userCart.data.UserCartArray.length > 0) {
                            common.setUserCart(userCart.data.UserCartArray);
                        } else {
                            let arr = [];
                            common.setUserCart(arr);
                        }
                    } else {
                        let arr = [];
                        common.setUserCart(arr);
                    }
                    setTimeout(() => {
                        const resetAction = StackActions.reset({
                            index: 0,
                            routeName: 'TabNavigator',
                            actions: [NavigationActions.navigate({ routeName: 'TabNavigator' })],
                        });
                        this.props.navigation.dispatch(resetAction)
                    }, 4800)
                });
            });
        } else {
            let arr = [];
            common.setUserCart(arr);
            setTimeout(() => {
                const resetAction = StackActions.reset({
                    index: 0,
                    routeName: 'Auth',
                    actions: [NavigationActions.navigate({ routeName: 'Auth' })],
                });
                this.props.navigation.dispatch(resetAction)
            }, 5000)
        }
    }

    render() {
        return (
            <ImageBackground
                style={{flex: 1}}
                resizeMode={'cover'}
                source={require('./../../assets/images/splash_gif.gif')}
            />
        );
    }
}
