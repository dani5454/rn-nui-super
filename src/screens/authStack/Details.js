import React from 'react';
import {View, Platform,SafeAreaView,Text,TouchableOpacity, StatusBar, StyleSheet} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import CommonDataManager from '../../service/Singlenton';
import Selector from '../../components/Selector';
import AppLoader from '../../components/AppLoader';
import HeaderWithBack from './../../components/HeaderWithBack';
import {hp, wp} from '../../UtilMethods/Utils';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? hp('6%') : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

const MyStatusBar = ({backgroundColor, ...props}) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
        <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
);

export default class Details extends React.Component {
    instance = CommonDataManager.getInstance();

    constructor(props) {
        super(props);

        this.state = {
            loader: false,
            price: this.props.navigation.state.params.price,
            marketName: this.props.navigation.state.params.marketName,
            location: '',
            time: '',
            comment: '',
            cupone: '',
            gotoPayScreen: false,
        };
    }

    _onLeftIconPress = () => {
        this.props.navigation.goBack();
    };

    _onAddressSelect = () => {
        this.props.navigation.navigate('SelectAddress');
    };

    _onStoreTiming = () => {
        this.props.navigation.navigate('StoreTiming');
    };

    _onComment = () => {
        this.props.navigation.navigate('Comment');
    };

    _onCupone = () => {
        this.props.navigation.navigate('Cupone');
    };

    _onWillFocus = (payload) => {
        this.setState({loader: true});
        let user = this.instance.getUser();

        if(Array.isArray(user.address)) {
            user.address.map((item) => {
                if(item.check) {
                    this.setState({location: item.addressAndNumber})
                }
            });
        }
        if(Array.isArray(user.comments)) {
            this.setState({comment: user.comments[0].selector})
        }
        if(user.timeSchedule) {
            this.setState({time: user.timeSchedule.time, gotoPayScreen: true})
        }
        this.setState({loader: false});
    };

    render() {
        return (
            <View style={styles.container}>
                <MyStatusBar backgroundColor="#D64B12" barStyle="light-content" />

                <NavigationEvents onWillFocus={payload => this._onWillFocus(payload)} />

                {AppLoader.renderLoading(this.state.loader)}

                <SafeAreaView>
                    <HeaderWithBack
                        show={true}
                        rightIcon={true}
                        text={'Rellena tus datos'}
                        icon={require('./../../assets/icon/ic_question-512.png')}
                        onLeftIcon={this._onLeftIconPress.bind(this)}
                    />
                </SafeAreaView>
                <View style={{flex: 1, marginTop: wp('10%'), flexDirection: 'column'}}>
                    <Selector
                        title={'Direccion de entrega'}
                        icon={require('./../../assets/icon/ic_location-512.png')}
                        selectorText={this.state.location}
                        onSelect={this._onAddressSelect.bind(this)}
                    />
                    <Selector
                        viewStyle={{marginTop: wp('8%')}}
                        icon={require('./../../assets/icon/ic_timer-512.png')}
                        selectorText={this.state.marketName}
                        isRightContent={true}
                        rightContentText={this.state.time}
                        title={'Horario de entrega'}
                        onSelect={this._onStoreTiming.bind(this)}
                    />
                    <Selector
                        viewStyle={{marginTop: wp('8%')}}
                        icon={require('./../../assets/icon/ic_setting-512.png')}
                        selectorText={this.state.comment}
                        title={'Opciones del pedido'}
                        onSelect={this._onComment.bind(this)}
                    />
                    <Selector
                        viewStyle={{marginTop: wp('8%')}}
                        icon={require('./../../assets/icon/ic_card-512.png')}
                        title={'Cupones'}
                        selectorText={'Aplicar cupon de descuento'}
                        onSelect={this._onCupone.bind(this)}
                    />
                    <View style={styles.separator}/>
                    <View style={{flex: 1, marginTop: wp('5%'), paddingLeft: wp('5%')}}>
                        {this._renderReceipt('Servico Nui Super + Envio', 'GRATIS', null, '#6c9f27')}
                        {this._renderReceipt('Total cupones', 'L. 0.00', wp('4%'), '#6c9f27')}
                        {this._renderReceipt('Compra', 'L. '+this.state.price, wp('4%'), '#6c9f27')}
                        {this._renderReceipt('TOTAL', 'L. '+this.state.price, wp('4%'), '#000000')}
                    </View>
                    <View style={{flex: 1}}>
                        {this._renderFooter()}
                    </View>
                </View>
            </View>
        );
    }

    _renderReceipt = (left, right, top, color) => {
        return (
            <View style={{justifyContent: 'space-between', flexDirection: 'row', marginTop: top}}>
                <Text>{left}</Text>
                <Text style={{right: wp('3%'), color: color}}>{right}</Text>
            </View>
        );
    };

    _renderFooter = () => {
        return(
            <TouchableOpacity
                activeOpacity={1}
                onPress={() => this.moveToPayment()}
                style={{flex: 1, justifyContent: 'flex-end'}}>
                <View style={{backgroundColor: '#6c9f27'}}>
                    <SafeAreaView>
                        <View style={{height: hp('6%'),justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{color: '#ffffff', fontSize: 18}}>lr a pago</Text>
                        </View>
                    </SafeAreaView>
                </View>
            </TouchableOpacity>
        );
    };

    moveToPayment = () => {
        const { gotoPayScreen } = this.state;
        if(gotoPayScreen) {
            this.props.navigation.navigate('Payment', {price: this.state.price, marketName: this.state.marketName})
        } else {
            alert('dirección faltante o horario de entrega');
        }
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    statusBar: {
        height: STATUSBAR_HEIGHT,
    },
    appBar: {
        backgroundColor:'#79B45D',
        height: APPBAR_HEIGHT,
    },
    content: {
        flex: 1,
        backgroundColor: '#33373B',
    },
    separator: {
        backgroundColor: 'rgb(200, 199, 204)',
        height: StyleSheet.hairlineWidth,
    },
});
