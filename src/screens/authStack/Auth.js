import React from 'react';
import {View,ImageBackground,Text,StyleSheet,TouchableOpacity} from 'react-native';
import {wp, hp} from '../../UtilMethods/Utils';
import FontFamily from './../../assets/styles/Styles';

export default class Landing extends React.Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ImageBackground
                source={require('./../../assets/images/SelectionScreen.jpg')}
                style={{ flex: 1 }}>
                <View style={[styles.container, styles.content]}>
                    <View style={styles.btnContainer}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('SignUp')}
                        >
                            <ImageBackground
                                source={require('./../../assets/images/button.png')}
                                style={[{
                                    width: wp('65%'),
                                    height: hp('7%')

                                }, styles.content]}
                            >
                                <Text style={[FontFamily.gothamRounded_bold, {color: '#ffffff'}]}>Crear cuenta</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('SignIn')}
                        >
                            <ImageBackground
                                source={require('./../../assets/images/button.png')}
                                style={[{
                                    width: wp('65%'),
                                    height: hp('7%')

                                }, styles.content]}
                            >
                                <Text style={[FontFamily.gothamRounded_bold, {color: '#ffffff'}]}>Iniciar sesión</Text>
                            </ImageBackground>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    content: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnContainer: {
        height: hp('20%'),
        justifyContent: 'space-between',
    }
});
