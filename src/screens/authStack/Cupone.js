import React from  'react';
import {SafeAreaView, StyleSheet, TextInput, View, Alert, StatusBar, Platform} from 'react-native';
import { wp, hp } from './../../UtilMethods/Utils';
import AppLoader from '../../components/AppLoader';
import CommonDataManager from '../../service/Singlenton';
import Service from './../../firebase/Firebase';
import AddAddressHeader from '../../components/AddAddressHeader';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? hp('6%') : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

const MyStatusBar = ({backgroundColor, ...props}) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
        <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
);

export default class Cupone extends React.Component {
    instance = CommonDataManager.getInstance();

    constructor(props) {
        super(props);

        this.state = {
            loader: false,
            cupone: ''
        };
    }

    _onLeftIconPress = () => {
        this.props.navigation.goBack();
    };

    _onRightIconPress = () => {
        this.setState({loader: true});
        Service._getCupones((cupones) => {
            if(!cupones.error) {
                this._validateCupone(cupones.data.cupones);
            } else {
                this.setState({loader: false}, () => {
                    setTimeout(() => {
                        alert(cupones.message);
                    }, 500)
                })
            }
        });
    };

    _validateCupone = (cupones) => {
        const { cupone } = this.state;
        let validate = cupones.filter((item) => {
            if(item === cupone) {
                return item;
            }
        });

        this.setState({loader: false}, () => {
            if(validate.length > 0) {
                //
            } else {
                setTimeout(() => {
                    this._throwError()
                }, 500);
            }
        });
    };

    _throwError = () => {
        Alert.alert(
            'Error de validación',
            'Lo sentimos, pero este cupón está\ndeshabilitado',
            [
                {
                    text: 'Continuar',
                    onPress: () => console.log('OK Pressed'),
                    style: 'destructive',
                },
            ],
            {cancelable: false},
        );
    };

    render() {
        return (
            <View style={styles.container}>
                <MyStatusBar backgroundColor="#D64B12" barStyle="light-content" />

                {AppLoader.renderLoading(this.state.loader)}

                <SafeAreaView>
                    <AddAddressHeader
                        text={'Cupones'}
                        isRight={true}
                        rightText={'Aplicar'}
                        isImage={true}
                        onLeftIcon={this._onLeftIconPress.bind(this)}
                        onRightIcon={this._onRightIconPress.bind(this)}
                    />
                </SafeAreaView>
                <View style={{flex: 1}}>
                    <View style={styles.searchSection}>
                        <View style={{flex: 1}}>
                            <TextInput
                                style={styles.input}
                                autoCorrect={false}
                                placeholder="Codigo del cupon"
                                onChangeText={(cupone) => {this.setState({cupone})}}
                                // underlineColorAndroid="transparent"
                                clearButtonMode='always'
                            />
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f7faf8'
    },
    statusBar: {
        height: STATUSBAR_HEIGHT,
    },
    appBar: {
        backgroundColor:'#79B45D',
        height: APPBAR_HEIGHT,
    },
    content: {
        flex: 1,
        backgroundColor: '#33373B',
    },
    searchSection: {
        flexDirection: 'row',
        width: wp('100%'),
        height: hp('5%'),
        marginLeft: wp('15%'),
        marginTop: hp('3%'),
    },
    input: {
        width: wp('80%'),
        flex: 1,
    },
});
