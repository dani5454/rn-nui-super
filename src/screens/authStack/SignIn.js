import React from 'react';
import {
    View,
    Text,
    TextInput,
    Image,
    ImageBackground,
    TouchableOpacity,
    SafeAreaView,
    StyleSheet,
    Modal,
    Dimensions, ActivityIndicator, Platform
} from 'react-native';
import service from './../../firebase/Firebase';
import { wp, hp } from './../../UtilMethods/Utils';
import {AccessToken, LoginManager} from "react-native-fbsdk";
import firebase from "react-native-firebase";
import {NavigationActions, StackActions} from "react-navigation";
import CommonDataManager from '../../service/Singlenton';
import {GoogleSignin, statusCodes} from "@react-native-community/google-signin";

GoogleSignin.configure({
    webClientId: '790311402678-nl0kjh0674h8oge4tohblevcbb18our8.apps.googleusercontent.com'
});

const width = Dimensions.get('window').width;

export default class SignIn extends React.Component {
    instance = CommonDataManager.getInstance();

    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            errorMessage: false,
            loader: false,
            errorMessaging: 'Bienvenido ',
            userName: '',
        }
    }

    _onFbLogin = () => {
        LoginManager.logInWithPermissions(['public_profile', 'email'])
            .then((result) => {
                if (result.isCancelled) {
                    console.log('canceled');
                } else {
                    return AccessToken.getCurrentAccessToken();
                }
            })
            .then((data) => {
                if(data !== undefined) {
                    this.setState({loader: true});
                    const cred = firebase.auth.FacebookAuthProvider.credential(data.accessToken);
                    this._fbWithFirebaseCredential(cred);
                }
            })
            .catch((err) => {
                console.log(err);
            })
    };

    _fbWithFirebaseCredential = (credentials) => {
        service._signUpWithCredentials(credentials, (currentUser) => {
            if(!currentUser.error) {
                if (currentUser.response.additionalUserInfo.isNewUser) {
                    this._setUpFacebookProfile(currentUser.response).done();
                } else {
                    this.setState({loader: false, userName: currentUser.response.user._user.displayName}, () => {
                        setTimeout(() => {
                            this.setState({errorMessage: true});
                        }, 300)
                    });
                }
            } else {
                alert(currentUser.errorMessage);
            }
        });
    };

    _setUpFacebookProfile = async (currentUser) => {
        await service._setUpUserFbProfile(currentUser.user._user.uid, currentUser.additionalUserInfo);
        // navigate to intro Screen
        this.setState({loader: false}, () => {
            this.moveToScreenWithStackReset('OnBoard');
        });
    };

    _onGoogleLogin = () => {
        try {
            GoogleSignin.hasPlayServices().done();
            GoogleSignin.signIn().then((res) => {
                console.log('GOOGLE-RESP---->>>>', res);
                this.setState({loader: true});
                const cred = firebase.auth.GoogleAuthProvider.credential(res.idToken);
                this._googleWithFirebaseCredential(cred);
            });
        } catch (error) {
            console.log(error);
            console.log(error.code);
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                // user cancelled the login flow
            } else if (error.code === statusCodes.IN_PROGRESS) {
                // operation (e.g. sign in) is in progress already
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                // play services not available or outdated
            } else {
                // some other error happened
            }
        }
    };

    _googleWithFirebaseCredential = (credentials) => {
        service._signUpWithCredentials(credentials, (currentUser) => {
            if(!currentUser.error) {
                if (currentUser.response.additionalUserInfo.isNewUser) {
                    this._setUpGoogleProfile(currentUser.response).done();
                } else {
                    // this.moveToScreenWithStackReset('TabNavigator');
                    this.setState({loader: false, userName: currentUser.response.user._user.displayName}, () => {
                        setTimeout(() => {
                            this.setState({errorMessage: true});
                        }, 300)
                    });
                }
            } else {
                this.setState({loader: false}, () => {
                    alert(currentUser.errorMessage);
                });
            }
        });
    };

    _setUpGoogleProfile = async (currentUser) => {
        await service._setUpUserGoogleProfile(currentUser.user._user.uid, currentUser.additionalUserInfo).done();
        // navigate to intro Screen
        this.setState({loader: false}, () => {
            this.moveToScreenWithStackReset('OnBoard');
        });
    };

    moveToScreenWithStackReset = (screenName) => {
        service._getUserProfile(firebase.auth().currentUser.uid,(user) => {
            if (!user.error) {
                this.instance.setUser(user.data);
            }
            this.setState({errorMessage: false}, () => {
                setTimeout(() => {
                    const resetAction = StackActions.reset({
                        index: 0,
                        routeName: screenName,
                        actions: [NavigationActions.navigate({routeName: screenName})],
                    });
                    this.props.navigation.dispatch(resetAction)
                }, 300)
            });
        });
    };

    getArray () {
        return [
            {
                Frutas_y_verduras: [
                    {
                        image: 'https://i.ibb.co/n0pq0B3/Manzana-Roja.jpg',
                        category_type: 'Frutas y verduras',
                        name: 'Manzana Roja',
                        description: 'Fruta fresca libra',
                        price: '20.00',
                    },
                    {
                        image: 'https://i.ibb.co/T1VjsQ5/Peras.jpg',
                        category_type: 'Frutas y verduras',
                        name: 'Peras',
                        description: 'Fruta fresca libra',
                        price: '40.00',
                    },
                    {
                        image: 'https://i.ibb.co/94x0Jx8/Melocotones.jpg',
                        category_type: 'Frutas y verduras',
                        name: 'Melocotones',
                        description: 'Fruta fresca libra',
                        price: '60.00',
                    },
                    {
                        image: 'https://i.ibb.co/NKB3gSx/Lechuga-Escarola.jpg',
                        category_type: 'Frutas y verduras',
                        name: 'Lechuga Escarola',
                        description: 'Verdura fresca',
                        price: '10.00',
                    },
                    {
                        image: 'https://i.ibb.co/yPSt9sn/Tomate.jpg',
                        category_type: 'Frutas y verduras',
                        name: 'Tamato',
                        description: 'Verdura fresca',
                        price: '8.00',
                    },
                ],
            },
            {
                Lacteos: [
                    {
                        image: 'https://i.ibb.co/p1B7jz4/Leche-Entera.jpg',
                        category_type: 'Lacteos',
                        name: 'Leche Entera',
                        description: 'Leche Sula lt',
                        price: '25.00',
                    },
                    {
                        image: 'https://i.ibb.co/F3YPvPZ/lala.jpg',
                        category_type: 'Lacteos',
                        name: 'Leche Descremada',
                        description: 'Leche Lala lt',
                        price: '30.00',
                    },
                    {
                        image: 'https://i.ibb.co/sR9w4Ng/Leche-Almendras.jpg',
                        category_type: 'Lacteos',
                        name: 'Leche Almendras',
                        description: 'Leche Almond lt',
                        price: '80.00',
                    },
                    {
                        image: 'https://i.ibb.co/zFGrwPV/stawbery.jpg',
                        category_type: 'Lacteos',
                        name: 'Yogurt',
                        description: 'Yogurt Fresa',
                        price: '8.00',
                    },
                    {
                        image: 'https://i.ibb.co/9VcyyWb/Yogurt.jpg',
                        category_type: 'Lacteos',
                        name: 'Yogurt',
                        description: 'Yogurt Natural',
                        price: '25.00',
                    },
                ],
            },
            {
                Carnes_y_mariscos: [
                    {
                        image: 'https://i.ibb.co/FmhDY4M/Carne-Res.jpg',
                        category_type: 'Carnes y mariscos',
                        name: 'Carne Res',
                        description: 'Filete res lb',
                        price: '75.00',
                    },
                    {
                        image: 'https://i.ibb.co/Wptk5Cr/Chuleta-de-Cerdo.jpg',
                        category_type: 'Carnes y mariscos',
                        name: 'Chuleta de Cerdo',
                        description: 'Chuleta fresca lb',
                        price: '40.00',
                    },
                    {
                        image: 'https://i.ibb.co/VHtqW6H/Pollo-Pechuga-Rey.jpg',
                        category_type: 'Carnes y mariscos',
                        name: 'Pollo Pechuga Rey',
                        description: 'Pechuga Fresca Lb',
                        price: '50.00',
                    },
                    {
                        image: 'https://i.ibb.co/sJFSSMv/Salmon.jpg',
                        category_type: 'Carnes y mariscos',
                        name: 'Salmon',
                        description: 'Salmon fresco lb',
                        price: '128.00',
                    },
                    {
                        image: 'https://i.ibb.co/qdtjt57/Camarones.jpg',
                        category_type: 'Carnes y mariscos',
                        name: 'Camarones',
                        description: 'Camarones jumbo',
                        price: '115.00',
                    },
                ],
            },
            {
                Panaderia_y_reposteria: [
                    {
                        image: 'https://i.ibb.co/6Y1c2Qx/Pan-Integral-Light.png',
                        category_type: 'Panaderia y reposteria',
                        name: 'Pan Integral',
                        description: 'Pan con semillas',
                        price: '35.00',
                    },
                    {
                        image: 'https://i.ibb.co/stXGs79/Pan-Blanco.png',
                        category_type: 'Panaderia y reposteria',
                        name: 'Pan Blanco',
                        description: 'Pan bimbo',
                        price: '30.00',
                    },
                    {
                        image: 'https://i.ibb.co/6Y1c2Qx/Pan-Integral-Light.png',
                        category_type: 'Panaderia y reposteria',
                        name: 'Pan Integral Light',
                        description: 'Pan de dieta',
                        price: '60.00',
                    },
                    {
                        image: 'https://i.ibb.co/9rJRvcC/bimbo-gansito-marinela.png',
                        category_type: 'Panaderia y reposteria',
                        name: 'Brownie Chocolate',
                        description: 'Brownie relleno',
                        price: '28.00',
                    },
                    {
                        image: 'https://i.ibb.co/YjRSXkv/ddd.png',
                        category_type: 'Panaderia y reposteria',
                        name: 'Chocolate',
                        description: 'Chocolate nestle',
                        price: '15.00',
                    },
                ]
            },
        ]
    }

    _La_Colonia () {
        return [
            {
                Frutas_y_verduras: [
                    {
                        image: 'https://i.ibb.co/n0pq0B3/Manzana-Roja.jpg',
                        category_type: 'Frutas y verduras',
                        name: 'Manzana Roja',
                        description: 'Fruta fresca libra',
                        price: '20.00',
                    },
                    {
                        image: 'https://i.ibb.co/T1VjsQ5/Peras.jpg',
                        category_type: 'Frutas y verduras',
                        name: 'Peras',
                        description: 'Fruta fresca libra',
                        price: '40.00',
                    },
                    {
                        image: 'https://i.ibb.co/94x0Jx8/Melocotones.jpg',
                        category_type: 'Frutas y verduras',
                        name: 'Melocotones',
                        description: 'Fruta fresca libra',
                        price: '60.00',
                    },
                ],
            },
            {
                Lacteos: [
                    {
                        image: 'https://i.ibb.co/p1B7jz4/Leche-Entera.jpg',
                        category_type: 'Lacteos',
                        name: 'Leche Entera',
                        description: 'Leche Sula lt',
                        price: '25.00',
                    },
                    {
                        image: 'https://i.ibb.co/sR9w4Ng/Leche-Almendras.jpg',
                        category_type: 'Lacteos',
                        name: 'Leche Entera',
                        description: 'Leche Almond lt',
                        price: '80.00',
                    },
                    {
                        image: 'https://i.ibb.co/F3YPvPZ/lala.jpg',
                        category_type: 'Lacteos',
                        name: 'Leche Descremada',
                        description: 'Leche Lala lt',
                        price: '30.00',
                    },
                ],
            },
            {
                Carnes_y_mariscos: [
                    {
                        image: 'https://i.ibb.co/FmhDY4M/Carne-Res.jpg',
                        category_type: 'Carnes y mariscos',
                        name: 'Carne Res',
                        description: 'Filete res lb',
                        price: '75.00',
                    },
                    {
                        image: 'https://i.ibb.co/VHtqW6H/Pollo-Pechuga-Rey.jpg',
                        category_type: 'Carnes y mariscos',
                        name: 'Pollo Pechuga Rey',
                        description: 'Pechuga Fresca Lb',
                        price: '50.00',
                    },
                    {
                        image: 'https://i.ibb.co/sJFSSMv/Salmon.jpg',
                        category_type: 'Carnes y mariscos',
                        name: 'Salmon',
                        description: 'Salmon fresco lb',
                        price: '128.00',
                    },
                    {
                        image: 'https://i.ibb.co/Wptk5Cr/Chuleta-de-Cerdo.jpg',
                        category_type: 'Carnes y mariscos',
                        name: 'Chuleta de Cerdo',
                        description: 'Chuleta fresca lb',
                        price: '40.00',
                    },
                ],
            },
            {
                Panaderia_y_reposteria: [
                    {
                        image: 'https://i.ibb.co/6Y1c2Qx/Pan-Integral-Light.png',
                        category_type: 'Panaderia y reposteria',
                        name: 'Pan Integral',
                        description: 'Pan con semillas',
                        price: '35.00',
                    },
                    {
                        image: 'https://i.ibb.co/stXGs79/Pan-Blanco.png',
                        category_type: 'Panaderia y reposteria',
                        name: 'Pan Blanco',
                        description: 'Pan bimbo',
                        price: '30.00',
                    },
                    {
                        image: 'https://i.ibb.co/6Y1c2Qx/Pan-Integral-Light.png',
                        category_type: 'Panaderia y reposteria',
                        name: 'Pan Integral Light',
                        description: 'Pan de dieta',
                        price: '60.00',
                    },
                    {
                        image: 'https://i.ibb.co/9rJRvcC/bimbo-gansito-marinela.png',
                        category_type: 'Panaderia y reposteria',
                        name: 'Brownie Chocolate',
                        description: 'Brownie relleno',
                        price: '28.00',
                    },
                    {
                        image: 'https://i.ibb.co/YjRSXkv/ddd.png',
                        category_type: 'Panaderia y reposteria',
                        name: 'Chocolate',
                        description: 'Chocolate nestle',
                        price: '15.00',
                    },
                ]
            },
        ]
    }

    _signIn = async () => {
        const {email, password} = this.state;
        if(email.length > 0 && password.length > 0) {
            this.setState({loader: true});
            service._signIn(email, password, async (user) => {
                if(!user.error) {
                    service._getUserProfile(user.response._user.uid, (resp) => {
                        this.setState({userName: resp.data.name});
                        this.setState({loader: false}, () => {
                            setTimeout(() => {
                                this.setState({errorMessage: true});
                            }, 300)
                        });
                    });
                } else {
                    this.setState({loader: false}, () => {
                        setTimeout(() => {
                            alert(user.errorMessage);
                        }, 300)
                    });
                }
            });
        }
    };

    render() {
        return (
            <ImageBackground
                style={{flex: 1}}
                source={require('./../../assets/images/sign-in.jpg')}
            >
                <SafeAreaView style={{flex: 1}}>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', margin: 10}}>
                        <Image
                            source={require('./../../assets/images/logo.png')}
                            style={{ width: 50, height: 50}}
                        />
                        <Text>REGISTRATE</Text>
                    </View>
                    <View style={{flex: 1}}>
                        <View style={{borderBottomWidth: 2, borderBottomColor: '#D64B12', marginLeft: 15, marginRight: 15}}>
                            <TextInput
                                placeholder={'Correo electrónico'}
                                autoCapitalize='none'
                                autoCorrect={false}
                                value={this.state.email}
                                keyboardType={'email-address'}
                                onChangeText={email => this.setState({email})}
                            />
                        </View>
                        <View style={{borderBottomWidth: 2,marginTop: Platform.OS === 'ios' ? 20 : null, borderBottomColor: '#D64B12', marginLeft: 15, marginRight: 15}}>
                            <TextInput
                                placeholder={'Contraseña'}
                                autoCapitalize='none'
                                secureTextEntry={true}
                                value={this.state.password}
                                onChangeText={password => this.setState({password})}
                            />
                        </View>
                        <View style={{flexDirection: 'row',marginTop: Platform.OS === 'ios' ? 20 : null}}>
                            <TouchableOpacity
                                onPress={() => this._signIn()}
                                style={{marginTop: 10, marginLeft: 15}}>
                                <View style={{backgroundColor: '#d64b12',borderRadius: 50,height: 30, width: wp('35%'), alignItems: 'center', justifyContent: 'center'}}>
                                    <Text style={{color: 'white'}}>INICIAR SESION</Text>
                                </View>
                            </TouchableOpacity>
                            <View style={{alignItems: 'flex-end',flex: 1,height: wp('13%')}}>
                                <Text style={{marginTop: wp('4%'), marginRight: wp('4%')}}>Olvide mi contraseña</Text>
                            </View>
                        </View>
                        <View style={{alignItems: 'center',marginTop: 10}}>
                            <Text>Iniciar sesión con:</Text>
                            <TouchableOpacity
                                onPress={() => this._onFbLogin()}
                                style={{backgroundColor: '#4267b2',marginTop: 20,borderRadius: 50,height: 35, width: wp('40%'), alignItems: 'center', justifyContent: 'center'}}>
                                <Text style={{color: 'white'}}>FACEBOOK</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this._onGoogleLogin()}
                                style={{backgroundColor: '#ea4335',marginTop: 20,borderRadius: 50,height: 35, width: wp('40%'), alignItems: 'center', justifyContent: 'center'}}>
                                <Text style={{color: 'white'}}>GOOGLE</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <Modal
                        // onRequestClose={() => this.setState({ errorMessage: false })}
                        animationType="fade"
                        transparent={true}
                        visible={this.state.loader}
                    >
                        <View style={styles.modalRootStyle}>
                            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                                <ActivityIndicator />
                            </View>
                        </View>
                    </Modal>
                    <Modal
                        // onRequestClose={() => this.setState({ errorMessage: false })}
                        animationType="fade"
                        transparent={true}
                        visible={this.state.errorMessage}
                    >
                        <View style={styles.modalRootStyle}>
                            <View style={styles.modalBoxStyle}>
                                <View style={{flex: 1}}>
                                    <View style={{alignItems: 'center', zIndex: 1}}>
                                        <View style={{
                                            top: wp('15%'),
                                            width: 50,
                                            height: 50, borderRadius: 50, zIndex: 1,
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                            backgroundColor: '#FF6F00'
                                        }}>
                                            <Image source={require('./../../assets/images/notification_logo.png')}
                                                   style={{
                                                       width: wp('12%'), height: wp('12%')
                                                   }}
                                            />
                                        </View>
                                    </View>
                                    <ImageBackground
                                        source={require('./../../assets/images/notification.png')}
                                        resizeMode={'contain'}
                                        style={{flex: 1}}
                                    >
                                        <View style={{marginTop: wp('8%'),flexDirection: 'column',flex: 1,alignItems: 'center', justifyContent: 'center'}}>
                                            <View>
                                                <Text>{this.state.errorMessaging}{this.state.userName}</Text>
                                            </View>
                                            <TouchableOpacity
                                                onPress={() => this.moveToScreenWithStackReset('TabNavigator')}
                                                style={{marginTop: wp('4%')}}>
                                                <Text style={{color: '#d64b12'}}>Continuar</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </ImageBackground>
                                </View>
                            </View>
                        </View>
                    </Modal>
                </SafeAreaView>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    modalRootStyle: {
        flex: 1,
        backgroundColor: "rgba(52, 52, 52, 0.8)",
        alignItems: 'center',
        justifyContent: "center",
    },
    modalBoxStyle: {
        width: width - 60,
        height: hp('30%'),
        borderRadius: 5,
    },
});
