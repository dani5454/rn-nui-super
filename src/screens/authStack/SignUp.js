import React from 'react';
import {
    View,
    Text,
    Dimensions,
    SafeAreaView,
    Modal,
    Image,
    ImageBackground,
    TextInput,
    TouchableOpacity,
    StyleSheet,
    ActivityIndicator, Platform
} from 'react-native';
import { GoogleSignin, statusCodes  } from '@react-native-community/google-signin';
import { StackActions, NavigationActions } from 'react-navigation';
import {AccessToken,  LoginManager } from "react-native-fbsdk";
import { wp, hp } from './../../UtilMethods/Utils';
import CommonDataManager from '../../service/Singlenton';
import service from './../../firebase/Firebase';
import firebase from 'react-native-firebase';
// import {object, string} from "yup";

GoogleSignin.configure({
    webClientId: '790311402678-nl0kjh0674h8oge4tohblevcbb18our8.apps.googleusercontent.com'
});

const width = Dimensions.get('window').width;

export default class SignUp extends React.Component {
    instance = CommonDataManager.getInstance();

    constructor(props) {
        super(props);

        this.state = {
            name: '',
            lastName: '',
            email: '',
            password: '',
            address: '',
            userName: '',
            screenName: 'OnBoard',
            confirm_pass: '',
            errorMessage: false,
            loader: false,
            errorMessaging: 'Usuario creado con éxito',
        }
    }

    _onFbLogin = () => {
        LoginManager.logInWithPermissions(['public_profile', 'email'])
            .then((result) => {
                if (result.isCancelled) {
                    console.log('canceled');
                } else {
                    return AccessToken.getCurrentAccessToken();
                }
            })
            .then((data) => {
                if(data !== undefined) {
                    this.setState({loader: true});
                    const cred = firebase.auth.FacebookAuthProvider.credential(data.accessToken);
                    this._fbWithFirebaseCredential(cred);
                }
            })
            .catch((err) => {
                console.log(err);
            })
    };

    _fbWithFirebaseCredential = (credentials) => {
        service._signUpWithCredentials(credentials, (currentUser) => {
            if(!currentUser.error) {
                if (currentUser.response.additionalUserInfo.isNewUser) {
                    this._setUpFacebookProfile(currentUser.response).done();
                } else {
                    this.setState({loader: false}, () => {
                        // this.moveToScreenWithStackReset('TabNavigator');
                        this.setState({screenName: 'TabNavigator',loader: false, errorMessaging: 'Bienvenido ', userName: currentUser.response.user._user.displayName}, () => {
                            setTimeout(() => {
                                this.setState({errorMessage: true});
                            }, 300)
                        });
                    });

                }
            } else {
                alert(currentUser.errorMessage);
            }
        });
    };

    _setUpFacebookProfile = async (currentUser) => {
        await service._setUpUserFbProfile(currentUser.user._user.uid, currentUser.additionalUserInfo);
        // navigate to intro Screen
        this.setState({loader: false}, () => {
            setTimeout(() => {
                this.setState({errorMessage: true});
            }, 300);
        });
        // this.moveToScreenWithStackReset('OnBoard');
    };

    _onGoogleLogin = () => {
        try {
            GoogleSignin.hasPlayServices().done();
            GoogleSignin.signIn().then((resp) => {
                this.setState({loader: true});
                const cred = firebase.auth.GoogleAuthProvider.credential(resp.idToken);
                this._googleWithFirebaseCredential(cred);
            }).catch((error) => {
                console.log(error);
            });
        } catch (error) {
            console.log(error);
            // if (error.code === statusCodes.SIGN_IN_CANCELLED) {
            //     // user cancelled the login flow
            // } else if (error.code === statusCodes.IN_PROGRESS) {
            //     // operation (e.g. sign in) is in progress already
            // } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
            //     // play services not available or outdated
            // } else {
            //     // some other error happened
            // }
        }
    };

    _googleWithFirebaseCredential = (credentials) => {
        service._signUpWithCredentials(credentials, (currentUser) => {
            if(!currentUser.error) {
                if (currentUser.response.additionalUserInfo.isNewUser) {
                    this._setUpGoogleProfile(currentUser.response).done();
                } else {
                    this.setState({loader: false}, () => {
                        // this.moveToScreenWithStackReset('TabNavigator');
                        this.setState({screenName: 'TabNavigator',loader: false, errorMessaging: 'Bienvenido ', userName: currentUser.response.user._user.displayName}, () => {
                            setTimeout(() => {
                                this.setState({errorMessage: true});
                            }, 300)
                        });
                    });
                }
            } else {
                alert(currentUser.errorMessage);
            }
        });
    };

    _setUpGoogleProfile = async (currentUser) => {
      await service._setUpUserGoogleProfile(currentUser.user._user.uid, currentUser.additionalUserInfo).done();
        // navigate to intro Screen
        this.setState({errorMessage: true});
        // this.moveToScreenWithStackReset('OnBoard');
    };

    _signUp = () => {
        // this.moveToScreenWithStackReset('OnBoard');
        const {name, lastName, email, password,address, confirm_pass} = this.state;
        if(email.length > 0 && password.length > 0) {
            this.setState({loader: true});
            service._signUp(email, password, async (user) => {
                if(!user.error) {
                    //Ser Up User Profile
                    await service._setUpUserProfile(user.response._user.uid, email, name, lastName, '', address);
                    this.setState({loader: false}, () => {
                       setTimeout(() => {
                           this.setState({errorMessage: true});
                       }, 300)
                    });
                } else {
                    this.setState({loader: false}, () => {
                        alert(user.errorMessage);
                    });
                }
            });
        } else {
            console.log('Please Enter Email & Password');
        }
    };

    moveToScreenWithStackReset = (screenName) => {
        service._getUserProfile(firebase.auth().currentUser.uid,(user) => {
            if (!user.error) {
                this.instance.setUser(user.data);
            }
            const resetAction = StackActions.reset({
                index: 0,
                routeName: screenName,
                actions: [NavigationActions.navigate({routeName: screenName})],
            });
            this.props.navigation.dispatch(resetAction)
        });
    };

    render() {
        return (
            <ImageBackground
                style={{flex: 1}}
                source={require('./../../assets/images/signup.jpg')}
            >
                <SafeAreaView style={{flex: 1}}>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', margin: 10}}>
                        <Image
                            source={require('./../../assets/images/logo.png')}
                            style={{ width: wp('15%'), height: wp('15%')}}
                        />
                        <Text>INICIAR SESION</Text>
                    </View>
                    <View style={{marginLeft: 15}}>
                        <Text>REGÍSTRATE</Text>
                    </View>
                    <View style={{flex: 1}}>
                        <View style={{flexDirection: 'row',marginTop: Platform.OS === 'ios' ? 20 : null}}>
                            <View style={{borderBottomWidth: 2, width: width / 2 - 27, borderBottomColor: '#d64b12', marginLeft: 15, marginRight: 15}}>
                                <TextInput
                                    placeholder={'Nombre'}
                                    autoCapitalize='none'
                                    onChangeText={name => this.setState({name})}
                                />
                            </View>
                            <View style={{borderBottomWidth: 2, width: width / 2 - 27, borderBottomColor: '#d64b12', marginLeft: 5, marginRight: 15}}>
                                <TextInput
                                    placeholder={'Apellido'}
                                    autoCapitalize='none'
                                    onChangeText={lastName => this.setState({lastName})}
                                />
                            </View>
                        </View>
                        <View style={{borderBottomWidth: 2,marginTop: Platform.OS === 'ios' ? hp('3%') : null, borderBottomColor: '#d64b12', marginLeft: 15, marginRight: 15}}>
                            <TextInput
                                placeholder={'Correo electrónico'}
                                autoCapitalize='none'
                                keyboardType={'email-address'}
                                onChangeText={email => this.setState({email})}
                            />
                        </View>
                        <View style={{borderBottomWidth: 2,marginTop: Platform.OS === 'ios' ? 20 : null, borderBottomColor: '#d64b12', marginLeft: 15, marginRight: 15}}>
                            <TextInput
                                placeholder={'Dirección'}
                                autoCapitalize='none'
                                onChangeText={address => this.setState({address})}
                            />
                        </View>
                        <View style={{borderBottomWidth: 2,marginTop: Platform.OS === 'ios' ? 20 : null, borderBottomColor: '#d64b12', marginLeft: 15, marginRight: 15}}>
                            <TextInput
                                placeholder={'Contraseña'}
                                autoCapitalize='none'
                                secureTextEntry={true}
                                onChangeText={password => this.setState({password})}
                            />
                        </View>
                        <View style={{borderBottomWidth: 2,marginTop: Platform.OS === 'ios' ? 20 : null, borderBottomColor: '#d64b12', marginLeft: 15, marginRight: 15}}>
                            <TextInput
                                placeholder={'Confirmar contraseña'}
                                autoCapitalize='none'
                                secureTextEntry={true}
                                onChangeText={confirm_pass => this.setState({confirm_pass})}
                            />
                        </View>
                        <TouchableOpacity
                            onPress={() => this._signUp()}
                            style={{marginTop: 10, marginLeft: 15}}>
                            <View style={{backgroundColor: '#d64b12',marginTop: 10,borderRadius: 50,height: 30, width: wp('35%'), alignItems: 'center', justifyContent: 'center'}}>
                                <Text style={{color: 'white'}}>CREAR CUENTA</Text>
                            </View>
                        </TouchableOpacity>
                        <View style={{alignItems: 'center',marginTop: 10}}>
                            <Text>Conectar con</Text>
                            <TouchableOpacity
                                onPress={() => this._onFbLogin()}
                                style={{backgroundColor: '#4267b2',marginTop: 20,borderRadius: 50,height: 35, width: wp('40%'), alignItems: 'center', justifyContent: 'center'}}>
                                <Text style={{color: 'white'}}>FACEBOOK</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this._onGoogleLogin()}
                                style={{backgroundColor: '#ea4335',marginTop: 20,borderRadius: 50,height: 35, width: wp('40%'), alignItems: 'center', justifyContent: 'center'}}>
                                <Text style={{color: 'white'}}>GOOGLE</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <Modal
                        // onRequestClose={() => this.setState({ errorMessage: false })}
                        animationType="fade"
                        transparent={true}
                        visible={this.state.loader}
                    >
                        <View style={styles.modalRootStyle}>
                            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                                <ActivityIndicator />
                            </View>
                        </View>
                    </Modal>
                    <Modal
                        // onRequestClose={() => this.setState({ errorMessage: false })}
                        animationType="fade"
                        transparent={true}
                        visible={this.state.errorMessage}
                    >
                        <View style={styles.modalRootStyle}>
                            <View style={styles.modalBoxStyle}>
                                <View style={{alignItems: 'center',zIndex: 1}}>
                                    <View style={{
                                        top: hp('28%'), width: 50,
                                        height: 50, borderRadius: 50, zIndex: 1, alignItems: 'center',
                                        justifyContent: 'center', backgroundColor: '#FF6F00'
                                    }}>
                                        <Image source={require('./../../assets/images/notification_logo.png')}
                                               style={{
                                                   width: wp('12%'), height: wp('12%')
                                               }}
                                        />
                                    </View>
                                </View>
                                <ImageBackground
                                    source={require('./../../assets/images/notification.png')}
                                    resizeMode={'contain'}
                                    style={{flex: 1}}
                                >
                                    <View style={{marginTop: wp('8%'),flexDirection: 'column',flex: 1,alignItems: 'center', justifyContent: 'center'}}>
                                        <View>
                                            <Text>{this.state.errorMessaging}{this.state.userName}</Text>
                                        </View>
                                        <TouchableOpacity
                                            onPress={() => this.moveToScreenWithStackReset(this.state.screenName)}
                                            style={{marginTop: wp('4%')}}>
                                            <Text style={{color: '#d64b12'}}>Continuar</Text>
                                        </TouchableOpacity>
                                    </View>
                                </ImageBackground>
                            </View>
                        </View>
                    </Modal>
                </SafeAreaView>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    modalRootStyle: {
        flex: 1,
        backgroundColor: "rgba(52, 52, 52, 0.8)",
        alignItems: 'center',
        justifyContent: "center",
    },
    modalBoxStyle: {
        width: width - 60,
        height: width + 90,
        borderRadius: 5,
    },
});

// let schema = object().shape({
//     email: string()
//         .email()
//         .required()
// });
