import React from 'react';
import {Platform, SafeAreaView, StatusBar, StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import AddAddressHeader from '../../components/AddAddressHeader';
import InputContainer from '../../components/InputContainer';
import CommonDataManager from '../../service/Singlenton';
import AppLoader from '../../components/AppLoader';
import { NavigationEvents } from 'react-navigation';
import Service from './../../firebase/Firebase';
import AsyncStorage from '@react-native-community/async-storage';
import {hp, wp} from '../../UtilMethods/Utils';
import Geolocation from "react-native-geolocation-service";

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? hp('6%') : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

const MyStatusBar = ({backgroundColor, ...props}) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
        <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
);

export default class AddAddress extends React.Component {
    instance = CommonDataManager.getInstance();
    constructor(props) {
        super(props);

        this.state = {
            loader: false,
            alias_address: '',
            address_number: '',
            floor_door: '',
            city: '',
            code: '',
            movil: '',
            comments: '',
            lat: '',
            lng: '',
            error: false
        };

    }

    _onLeftIconPress = () => {
        this.props.navigation.goBack();
    };

    componentDidMount() {
        this._getLocation();
    }

    _getLocation = () => {
        Geolocation.getCurrentPosition(
            (position) => {
                this.setState({
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                    error: false
                });
            },
            (error) => {
                this.setState({error: true});
                console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
    };

    _onRightIconPress = () => {
        let user = this.instance.getUser();
        const {alias_address,address_number,floor_door,
            city,code,movil,comments} = this.state;
        if(alias_address.length === 0) {
            alert('por favor llene Alias para la dirección');
            return;
        }
        if(address_number.length === 0) {
            alert('por favor Dirección y número');
            return;
        }
        if(floor_door.length === 0) {
            alert('por favor Piso y puerta');
            return;
        }
        if(city.length === 0) {
            alert('por favor Ciudad');
            return;
        }
        if(code.length === 0) {
            alert('por favor Código postal');
            return;
        }
        if(movil.length === 0) {
            alert('por favor Móvil');
            return;
        }
        if(comments.length === 0) {
            alert('por favor Comentarios');
            return;
        }

        this.setState({loader: true});

        let checked = !Array.isArray(user.address);

        let addressArray = [];
        addressArray.push({alias: alias_address,
            addressAndNumber: address_number,
            floorAndDoor: floor_door,
            city: city,
            lat: this.state.lat,
            lng: this.state.lng,
            code: code,
            movil: movil,
            comments: comments,
            check: checked
        });

        if(Array.isArray(user.address)) {
            let arr = [ ...user.address, ...addressArray];
            this._pushUserAddress(arr, user,true);
        } else {
            this._pushUserAddress(addressArray,user, false);
        }

    };

    _pushUserAddress = (userAddress, userArr, arrayCase) => {
        Service._updateUserAddress(userAddress, (resp) => {
            if(!resp.error) {
                this.setState({loader: false }, async () => {
                    if (arrayCase) {
                        userArr.address = userAddress;
                        await this.instance.setUser(userArr);
                        this.props.navigation.goBack();
                    } else {
                        userArr.address = userAddress;
                        await this.instance.setUser(userArr);
                        this.props.navigation.goBack();
                    }
                });
            } else {
                alert('error de servidor interno');
            }
        });
    };

    _onWillFocus = async (payload) => {
        const value = await AsyncStorage.getItem('@saveUserAddress');
        if(value !== null) {
            let address = JSON.parse(value);
            this.setState({
                alias_address: address.address,
                lat: address.latitude,
                lng: address.longitude,
            });
            await AsyncStorage.removeItem('@saveUserAddress');
        }
    };

    render() {
        return (
            <View style={styles.container}>
                <MyStatusBar backgroundColor="#D64B12" barStyle="light-content" />

                <NavigationEvents onWillFocus={payload => this._onWillFocus(payload)} />

                {AppLoader.renderLoading(this.state.loader)}

                <SafeAreaView>
                    <AddAddressHeader
                        text={'Añadir dirección'}
                        isRight={true}
                        rightText={'Guardar'}
                        onLeftIcon={this._onLeftIconPress.bind(this)}
                        onRightIcon={this._onRightIconPress.bind(this)}
                    />
                </SafeAreaView>
                <View style={styles.midContainer}>
                    <InputContainer
                        placeholder={'Alias para la dirección'}
                        value={this.state.alias_address}
                        onChangeText={alias_address => this.setState({alias_address})}
                    />
                    <InputContainer
                        placeholder={'Dirección y número'}
                        inputStyle={{marginTop: wp('5%')}}
                        onChangeText={address_number => this.setState({address_number})}
                    />
                    <InputContainer
                        placeholder={'Piso y puerta'}
                        inputStyle={{marginTop: wp('5%')}}
                        onChangeText={floor_door => this.setState({floor_door})}
                    />
                    <View style={styles.inputContainer}>
                        <InputContainer
                            placeholder={'Ciudad'}
                            inputStyle={{width: wp('43%')}}
                            onChangeText={city => this.setState({city})}
                        />
                        <InputContainer
                            placeholder={'Código postal'}
                            inputStyle={{width: wp('43%')}}
                            onChangeText={code => this.setState({code})}
                        />
                    </View>
                    <InputContainer
                        placeholder={'Movil'}
                        inputStyle={{marginTop: wp('5%')}}
                        onChangeText={movil => this.setState({movil})}
                    />
                    <InputContainer
                        placeholder={'Comentarios'}
                        inputStyle={{marginTop: wp('5%')}}
                        onChangeText={comments => this.setState({comments})}
                    />
                    <TouchableOpacity
                        style={{marginTop: hp('4%')}}
                        onPress={() => this.props.navigation.navigate('Map')}
                    >
                        <Text>Ir a mapas</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    statusBar: {
        height: STATUSBAR_HEIGHT,
    },
    appBar: {
        backgroundColor:'#79B45D',
        height: APPBAR_HEIGHT,
    },
    content: {
        flex: 1,
        backgroundColor: '#33373B',
    },
    midContainer: {
        flex: 1,
        paddingLeft: wp('4%'),
        paddingRight: wp('4%'),
        marginTop: wp('5%'),
        flexDirection: 'column'
    },
    inputContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: wp('5%')
    }
});
