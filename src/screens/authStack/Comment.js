import React from  'react';
import {SafeAreaView, StyleSheet, ScrollView, TouchableOpacity, TextInput, Image, Text, View, StatusBar, Platform} from 'react-native';
import CommonDataManager from '../../service/Singlenton';
import AddAddressHeader from '../../components/AddAddressHeader';
import Service from './../../firebase/Firebase';
import { wp, hp } from './../../UtilMethods/Utils';
import AppLoader from '../../components/AppLoader';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? hp('6%') : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

const MyStatusBar = ({backgroundColor, ...props}) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
        <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
);

export default class Comment extends React.Component {
    instance = CommonDataManager.getInstance();

    constructor(props) {
        super(props);

        this.state = {
            loader: false,
            checkOne: false,
            checkTwo: false,
            checkThree: true,
            commentPlaceholder: 'Ejemplo: Quiero los tomates maduros; el jamon\ndulce fino, llamarme antes de la entrega...',
            comment: ''
        };

    }

    componentDidMount() {
        let user = this.instance.getUser();
        if(Array.isArray(user.comments)) {
            if(user.comments[0].selector === 'Me llame por') {
                this._selectCondition(1);
            } else if(user.comments[0].selector === 'Reemplace por') {
                this._selectCondition(2);
            } else if(user.comments[0].selector === 'No sustituir') {
                this._selectCondition(3);
            }
            this.setState({comment: user.comments[0].comment});
        }
    }

    _onLeftIconPress = () => {
        this.props.navigation.goBack();
    };

    _onRightIconPress = () => {
        const { comment,checkOne,checkTwo,checkThree } = this.state;
        let cat = '', arr = [];
        if(checkOne) {
            cat = 'Me llame por';
        }
        if (checkTwo) {
            // alert('Por favor seleccione categoría de producto');
            cat = 'Reemplace por';
        }
        if (checkThree){
            cat = 'No sustituir';
        }

        this.setState({ loader: true});
        arr.push({comment: comment, selector: cat});
        Service._updateComments(arr, (resp) => {
            if(!resp.error) {
                this._updateComment(arr);
            } else {
                this.setState({ loader: false }, () => {
                    setTimeout(() => {
                        alert(resp.message);
                    }, 500)
                });
            }
        })
    };

    _updateComment = (commentArray) => {
        let user = this.instance.getUser();
        user.comments = commentArray;
        this.instance.setUser(user);
        this.setState({loader: false}, () => {
            setTimeout(() => {
                this.props.navigation.goBack()
            }, 500);
        });
    };

    render() {
        return (
            <View style={styles.container}>

                <MyStatusBar backgroundColor="#D64B12" barStyle="light-content" />

                {AppLoader.renderLoading(this.state.loader)}

                <SafeAreaView>
                    <AddAddressHeader
                        text={'Opciones del pedido'}
                        isImage={true}
                        isRight={true}
                        rightText={'Guardar'}
                        onLeftIcon={this._onLeftIconPress.bind(this)}
                        onRightIcon={this._onRightIconPress.bind(this)}
                    />
                </SafeAreaView>

                <ScrollView contentContainerStyle={styles.container}>
                    <View style={{flex:1, marginTop: hp('6%')}}>
                        <View style={styles.leftMargin}>
                            <Text style={styles.description}>Si no esta disponible algun producto, quiero que</Text>
                            <Text style={styles.description}>mi #Nui Super...</Text>
                        </View>
                        <View style={[styles.separator, {marginTop: wp('3%')}]}/>
                        <View style={{backgroundColor: '#ffffff',marginTop: hp('4%')}}>
                            <View style={styles.checkContainer}>
                                {this.selector('Me llame por ofrecere alternativas', this.state.checkOne, 1)}
                                {this.selector('Reemplace por uno similar', this.state.checkTwo, 2)}
                                {this.selector('No sustituya el producto', this.state.checkThree, 3)}
                            </View>
                        </View>
                        <View style={[styles.leftMargin, {marginTop: hp('5%')}]}>
                            <Text style={styles.description}>Comentarios sobre el pedido para tu</Text>
                            <Text style={styles.description}>#Nui Super o la entrega (Opcional)</Text>
                        </View>
                        <View style={[styles.separator, {marginTop: hp('4%')}]} />

                        <View style={{borderWidth: .2,backgroundColor: '#ffffff',height: hp('15%'),  marginTop: hp('5%')}}>
                            <TextInput
                                style={{flex: 1, fontSize: 18, marginLeft: 5}}
                                placeholder={this.state.commentPlaceholder}
                                multiline
                                value={this.state.comment}
                                onChangeText={(comment) => this.setState({comment})}
                            />
                        </View>

                        <View style={{alignItems: 'center',marginTop: hp('5%'), justifyContent: 'center'}}>
                            <TouchableOpacity
                                onPress={() => this._onRightIconPress()}
                                style={styles.guardar}>
                                <Text style={{color: '#ffffff'}}>Guardar</Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }

    _selectCondition = (index) => {
        if(index === 1) {
            this.setState({
                checkOne: !this.state.checkOne,
                checkTwo: false,
                checkThree: false,

            });
        } else if(index === 2) {
            this.setState({
                checkOne: false,
                checkTwo: !this.state.checkTwo,
                checkThree: false,

            });
        } else if (index === 3) {
            this.setState({
                checkOne: false,
                checkTwo: false,
                checkThree: !this.state.checkThree,

            });
        }
    };

    selector = (text, selector, index) => {
        return(
            <TouchableOpacity
                onPress={() => this._selectCondition(index)}
                style={{flexDirection: 'row', alignItems: 'center'}}>
                <Image
                    source={require('./../../assets/icon/ic_check-512.png')}
                    style={{tintColor: selector?'#79B45D':'#000000',width:wp('5%') ,height:wp('5%')}}
                />
                <Text style={{left: wp('5%')}}>{text}</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f7faf8'
    },
    statusBar: {
        height: STATUSBAR_HEIGHT,
    },
    appBar: {
        backgroundColor:'#79B45D',
        height: APPBAR_HEIGHT,
    },
    content: {
        flex: 1,
        backgroundColor: '#33373B',
    },
    leftMargin: {
      marginLeft: wp('3%'),
    },
    description: {
        opacity: .7,
        fontSize:16
    },
    separator: {
        backgroundColor: 'rgb(200, 199, 204)',
        height: StyleSheet.hairlineWidth,
    },
    checkContainer: {
        backgroundColor: '#ffffff',
        width: wp('100%'),
        padding: 5,
        height: hp('15%'),
        marginLeft: wp('8%'),
        flexDirection: 'column',
        justifyContent: 'space-between'
    },
    guardar: {
        width: wp('25%'),
        height: wp('8%'),
        borderRadius: 10,
        backgroundColor: '#79B45D',
        alignItems: 'center',
        justifyContent: 'center'
    }
});
