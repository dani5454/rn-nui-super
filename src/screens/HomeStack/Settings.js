import React from 'react';
import {View, Text, Dimensions, Image, ImageBackground, TextInput, TouchableOpacity} from 'react-native';
import {NavigationActions, StackActions} from "react-navigation";
import Service from './../../firebase/Firebase';

export default class Settings extends React.Component {

    _logOut = async () => {
        Service._signOut().done();
        const resetAction = StackActions.reset({
            index: 0,
            routeName: 'Auth',
            actions: [NavigationActions.navigate({ routeName: 'Auth' })],
        });
        this.props.navigation.dispatch(resetAction)
    };

    render() {
        return(
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <TouchableOpacity
                    onPress={() => this._logOut()}
                >
                    <Text>LogOut</Text>

                </TouchableOpacity>
            </View>
        );
    }
}