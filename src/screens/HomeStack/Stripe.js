import React from 'react';
import {
    Image,
    Platform,
    SafeAreaView,
    TextInput,
    StatusBar,
    StyleSheet,
    Text, TouchableWithoutFeedback,
    Switch, Keyboard,
    View, TouchableOpacity,
} from 'react-native';
import {hp, wp} from '../../UtilMethods/Utils';
import axios from 'axios';
import qs from 'qs';
import AppLoader from '../../components/AppLoader';
import CommonDataManager from '../../service/Singlenton';
import HeaderWithBack from '../../components/HeaderWithBack';
import Service from "../../firebase/Firebase";

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? hp('6%') : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

const MyStatusBar = ({backgroundColor, ...props}) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
        <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
);

export default class Stripe extends React.Component {
    instance = CommonDataManager.getInstance();

    constructor(props) {
        super(props);

        this.state = {
            loader: false,
            marketName:this.props.navigation.state.params.marketName,
            price:this.props.navigation.state.params.price,
            switchVal: true,
            activeBtn: false,
            cardNumber: '',
            month: '',
            year: '',
            cvc: '',
        };

        // this.stripePayment('4242424242424242','09','2023','123').then((StripeResp) => this.processResponse(StripeResp)
        //     .then((jsonResponse)=> {
        //         console.log('jsonResponse---->>', jsonResponse);
        //     }));

    }

    processResponse = (response) => {
        const statusCode = response.status;
        const data = response.json();

        return Promise.all([statusCode, data]).then(res => ({
            statusCode: res[0],
            res: res[1]
        }));
    };

    stripePayment(cardNumber, month, year, cvc) {

        let cardDetails = {
            "card[number]": cardNumber,
            "card[exp_month]": month,
            "card[exp_year]": year,
            "card[cvc]": cvc
        };

        let formBody = [];
        for (let property in cardDetails) {
            let encodedKey = encodeURIComponent(property);
            let encodedValue = encodeURIComponent(cardDetails[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");

        return fetch('https://api.stripe.com/v1/tokens', {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Basic ' + 'cGtfdGVzdF9VS0xNMUt6cWZ5bldiTnc1Z1BHbTVlRkkwMEpacnlDM2N0OnNrX3Rlc3RfRDBYVFY4aGRYWXlYNTdYODFrYjZwMUI2MDA1NHJTZUNaQg=='
            },
            body: formBody
        });

    }

    _onLeftIconPress = () => {
        this.props.navigation.goBack();
    };

    _onStripePayment = () => {
        const { cardNumber, month, year, cvc } = this.state;
        console.log('ardNumber', cardNumber.length);
        if(cardNumber.length < 16) {
            alert('por favor ingrese el número de tarjeta correcto');
            return;
        }
        if(month.length < 1) {
            alert('el mes no es el formato correcto');
            return;
        }
        if(year.length < 4) {
            alert('año no es el formato correcto');
            return;
        }
        if(cvc.length < 3) {
            alert('cvc no es el formato correcto');
            return;
        }

        this.setState({loader: true});

        this.stripePayment(cardNumber,month,year,cvc).then((StripeResp) => this.processResponse(StripeResp)
            .then((jsonResponse)=> {
                if(typeof jsonResponse.res.error !== "undefined") {
                    this.setState({loader: false}, () => {
                        setTimeout(() => {
                            alert(jsonResponse.res.error.message);
                        }, 500)
                    });
                } else {
                    this.stripeCreateCharge(jsonResponse.res.id,this.state.price).then((chargeResp) => {
                        if (chargeResp.status === 200) {
                            this._removeCartData(chargeResp.data);
                        }
                    });
                }
            }));
    };

    _removeCartData = (StripeResponse) => {
        let cart = this.instance.getUserCart();
        let user = this.instance.getUser();
        Service._getOrderId((resp) => {
            if(!resp.error) {
                let orderID = resp.data.orderId + 1;
                Service._setUserOrder(orderID,user, cart,StripeResponse, (resp) => {
                    if(!resp.error) {
                        this.updateOrderId(orderID);
                    } else {
                        this.props.navigation.goBack();
                    }
                })
            }
        });
    };

    updateOrderId = (orderId) => {
        Service._updateOrderId(orderId, (resp) => {
            if(!resp.error) {
                this._removeCart()
            }
        });
    };

    _removeCart = () => {
        let array = [];
        Service._removeCart((resp) => {
            if(!resp.error) {
                this.instance.setUserCart(array);
                this.setState({loader: false});
                this.props.navigation.navigate('MyCartItem',{myCartItem: [], marketName: this.state.marketName, message: 'pago detectado con éxito'});
            }
        });
    };

    stripeCreateCharge(token, amount) {
        return axios.post(
            'https://api.stripe.com/v1/charges',
            qs.stringify({
                source: token,
                amount: amount,
                currency: 'usd',
                description: 'Purchase detox items',
            }),
            {
                headers: {
                    'Authorization': 'Bearer ' + 'sk_test_D0XTV8hdXYyX57X81kb6p1B60054rSeCZB',
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }
        );
    }

    render() {
        return (
            <TouchableWithoutFeedback
                onPress={() => {Keyboard.dismiss()}}
                style={styles.container}>
                <View style={styles.container}>
                    <MyStatusBar backgroundColor="#D64B12" barStyle="light-content" />

                    {AppLoader.renderLoading(this.state.loader)}

                    <SafeAreaView>
                        <HeaderWithBack
                            text={'Pago'}
                            rightIcon={true}
                            icon={require('./../../assets/icon/ic_question-512.png')}
                            onLeftIcon={this._onLeftIconPress.bind(this)}
                        />
                    </SafeAreaView>

                    <View style={styles.container}>
                        <View style={{
                            marginTop: hp('4%'),
                            height: hp('4%'),
                            marginLeft: wp('5%'),
                            flexDirection: 'row'
                        }}>
                            <View style={[styles.input, {width: wp('55%')}]}>
                                <Image source={require('./../../assets/icon/ic_card-512.png')}
                                       style={{resizeMode: 'contain',height: wp('5%'), width: wp('5%'), tintColor: '#D64B12'}}
                                />
                                <TextInput
                                    style={{width: wp('55%'), paddingLeft: 5}}
                                    maxLength={16}
                                    keyboardType={'number-pad'}
                                    placeholder={'N tarjeta'}
                                    onChangeText={cardNumber => this.setState({cardNumber})}
                                />
                            </View>
                            <View style={[styles.input, {marginLeft: wp('4%'),width: wp('14%')}]}>
                                <TextInput
                                    style={{width: wp('14%'), paddingLeft: 5}}
                                    maxLength={2}
                                    keyboardType={'number-pad'}
                                    placeholder={'MM'}
                                    onChangeText={month => this.setState({month})}
                                />
                            </View>
                            <View style={[styles.input, {right: 5}]}>
                                <Text style={{opacity: .5, color: 'grey'}}>/</Text>
                            </View>
                            <View style={[styles.input, {width: wp('14%')}]}>
                                <TextInput
                                    style={{width: wp('14%'), paddingLeft: 5}}
                                    maxLength={4}
                                    keyboardType={'number-pad'}
                                    placeholder={'AA'}
                                    onChangeText={year => this.setState({year})}
                                />
                            </View>
                        </View>
                        <View style={styles.container}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between'}}>
                                <View style={[styles.input, {marginLeft: wp('5%'),width: wp('14%')}]}>
                                    <TextInput
                                        style={{width: wp('14%'), paddingLeft: 5}}
                                        keyboardType={'number-pad'}
                                        maxLength={3}
                                        placeholder={'CVC'}
                                        onChangeText={cvc => this.setState({cvc})}
                                    />
                                </View>
                                <View style={{top: 5,right: wp('6%'), flexDirection: 'row', alignItems: 'center'}}>
                                    <Text style={{fontWeight: 'bold', opacity: .6, color: 'grey', right: 10, fontSize: 18}}>Guardar tarjeta</Text>
                                    <Switch
                                        onValueChange = {(switchVal) => this.setState({switchVal})}
                                        value = {this.state.switchVal}
                                    />
                                </View>
                            </View>
                            <View style={[ styles.container, {flexDirection: 'column', justifyContent: 'flex-end'}]}>
                                {this._renderReceipt('Servico Mamashopper + Envio', 'GRATIS', null, '#6c9f27')}
                                {this._renderReceipt('Total cupones', 'L. 0.00', wp('4%'), '#6c9f27')}
                                {this._renderReceipt('Compra', 'L. '+128, wp('4%'), '#6c9f27')}
                                {this._renderReceipt('TOTAL', 'L. '+128, wp('4%'), '#000000')}
                                {/*<View style={{height: 10}}/>*/}
                                {this._renderFooter()}
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        );
    }

    _renderReceipt = (left, right, top, color) => {
        return (
            <View style={{justifyContent: 'space-between', flexDirection: 'row', marginTop: top}}>
                <Text style={{left: wp('5%')}}>{left}</Text>
                <Text style={{right: wp('5%'), color: color}}>{right}</Text>
            </View>
        );
    };

    _renderFooter = () => {
        return(
            <TouchableOpacity
                activeOpacity={1}
                onPress={() => this._onStripePayment()}
            >
                <View style={{backgroundColor: '#6c9f27', opacity: .5}}>
                    <SafeAreaView>
                        <View style={{height: hp('6%'),justifyContent: 'center', alignItems: 'center'}}>
                            <Text style={{color: '#ffffff', fontSize: 18}}>Confirmar pedido</Text>
                        </View>
                    </SafeAreaView>
                </View>
            </TouchableOpacity>
        );
    };

}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    statusBar: {
        height: STATUSBAR_HEIGHT,
    },
    appBar: {
        backgroundColor:'#79B45D',
        height: APPBAR_HEIGHT,
    },
    content: {
        flex: 1,
        backgroundColor: '#33373B',
    },
    input: {
        borderBottomWidth: .5,
        borderBottomColor: 'grey',
        flexDirection: 'row',
        alignItems: 'center'
    }
});
