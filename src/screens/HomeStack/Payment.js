import React from 'react';
import {View, Platform,SafeAreaView,Text,TouchableOpacity, StatusBar, StyleSheet} from 'react-native';
import HeaderWithBack from './../../components/HeaderWithBack';
import CommonDataManager from '../../service/Singlenton';
import {hp, wp} from '../../UtilMethods/Utils';
import AppLoader from '../../components/AppLoader';
import Service from "../../firebase/Firebase";

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? hp('6%') : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

const MyStatusBar = ({backgroundColor, ...props}) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
        <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
);

export default class Payment extends React.Component {
    instance = CommonDataManager.getInstance();

    constructor(props) {
        super(props);

        this.state = {
            price: this.props.navigation.state.params.price,
            marketName: this.props.navigation.state.params.marketName,
            accessToken: '',
            approvalUrl: '',
            paymentId: '',
            loader: false,
        }
    }

    componentDidMount() {
    }

    _onLeftIconPress = () => {
        this.props.navigation.goBack();
    };

    render() {
        return (
            <View style={styles.container}>
                <MyStatusBar backgroundColor="#D64B12" barStyle="light-content" />

                {AppLoader.renderLoading(this.state.loader)}

                <SafeAreaView>
                    <HeaderWithBack
                        text={'Pago'}
                        rightIcon={true}
                        icon={require('./../../assets/icon/ic_question-512.png')}
                        onLeftIcon={this._onLeftIconPress.bind(this)}
                    />
                </SafeAreaView>

                <View style={{flex: 1}}>

                    <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: hp('10%')}}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Paypal', {price: this.state.price, marketName: this.state.marketName})}
                            style={{alignItems: 'center',justifyContent: 'center',borderWidth: .5,width: wp('30%'), height: hp('6%')}}>
                            <Text>Pagar con PayPal</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: hp('10%')}}>
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate('Stripe', {price: this.state.price, marketName: this.state.marketName})}
                            style={{alignItems: 'center',justifyContent: 'center',borderWidth: .5,width: wp('30%'), height: hp('6%')}}>
                            <Text>Pagar con Srtipe</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: hp('10%')}}>
                        <TouchableOpacity
                            onPress={() => this._emptyCart()}
                            style={{alignItems: 'center',justifyContent: 'center',borderWidth: .5,width: wp('30%'), height: hp('6%')}}>
                            <Text>Contra reembolso</Text>
                        </TouchableOpacity>
                    </View>
                    {/*{this._renderReceipt('Servico Mamashopper + Envio', 'GRATIS', null, '#6c9f27')}*/}
                    {/*{this._renderReceipt('Total cupones', 'L. 0.00', wp('4%'), '#6c9f27')}*/}
                    {/*{this._renderReceipt('Compra', 'L. '+this.state.price, wp('4%'), '#6c9f27')}*/}
                    {/*{this._renderReceipt('TOTAL', 'L. '+this.state.price, wp('4%'), '#000000')}*/}
                </View>

            </View>
        );
    }

    _emptyCart = () => {
        this._removeCartData('Pay on cash')
    };

    _removeCartData = (StripeResponse) => {
        this.setState({loader: true});
        let cart = this.instance.getUserCart();
        let user = this.instance.getUser();
        Service._getOrderId((resp) => {
            if(!resp.error) {
                let orderID = resp.data.orderId + 1;
                Service._setUserOrder(orderID,user, cart,StripeResponse, (resp) => {
                    if(!resp.error) {
                        this.updateOrderId(orderID);
                    } else {
                        this.props.navigation.goBack();
                    }
                })
            }
        });
    };

    updateOrderId = (orderId) => {
        Service._updateOrderId(orderId, (resp) => {
            if(!resp.error) {
                this._removeCart()
            }
        });
    };

    _removeCart = () => {
        let array = [];
        Service._removeCart((resp) => {
            if(!resp.error) {
                this.instance.setUserCart(array);
                this.setState({loader: false});
                this.props.navigation.navigate('MyCartItem',{myCartItem: [], marketName: this.state.marketName, message: 'pago detectado con éxito'});
            }
        });
    };

    _renderReceipt = (left, right, top, color) => {
        return (
            <View style={{justifyContent: 'space-between', flexDirection: 'row', marginTop: top}}>
                <Text>{left}</Text>
                <Text style={{right: wp('3%'), color: color}}>{right}</Text>
            </View>
        );
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    statusBar: {
        height: STATUSBAR_HEIGHT,
    },
    appBar: {
        backgroundColor:'#79B45D',
        height: APPBAR_HEIGHT,
    },
    content: {
        flex: 1,
        backgroundColor: '#33373B',
    },
});
