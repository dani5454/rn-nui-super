import React from 'react';
import {Platform, SafeAreaView, Text, StatusBar,TouchableOpacity, StyleSheet, FlatList, View} from 'react-native';
import {hp, wp} from '../../UtilMethods/Utils';
import moment from 'moment';
import AppLoader from '../../components/AppLoader';
import CommonDataManager from '../../service/Singlenton';
import Service from './../../firebase/Firebase';
import AddAddressHeader from '../../components/AddAddressHeader';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? hp('6%') : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

const MyStatusBar = ({backgroundColor, ...props}) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
        <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
);

export default class StoreTiming extends React.Component {
    instance = CommonDataManager.getInstance();

    constructor(props) {
        super(props);

        this.state = {
            loader: false,
            dateArray: [],
            currentMonth: '',
            timeSchedule: [
                {
                    time: '13:00 a 14:00',
                    price: '0.00'
                },
                {
                    time: '14:00 a 15:00',
                    price: '0.00'
                },
                {
                    time: '15:00 a 16:00',
                    price: '0.00'
                },
                {
                    time: '16:00 a 17:00',
                    price: '0.00'
                },
                {
                    time: '17:00 a 18:00',
                    price: '0.00'
                },
                {
                    time: '18:00 a 19:00',
                    price: '0.00'
                },
                {
                    time: '20:00 a 21:00',
                    price: '0.00'
                },
                {
                    time: '21:00 a 22:00',
                    price: '0.00'
                },
            ]
        }
    }

    _onLeftIconPress = () => {
        this.props.navigation.goBack();
    };

    async componentDidMount() {

        let date = new Date;
        let monthArray = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

        let month = date.getMonth();

        month = monthArray[month];

        this.setState({currentMonth: month});

        let { dateArray } = this.state;
        let startDate = new Date(moment.now());
        let endDate = "", offset = 0;
        while(offset < 7){
            if(offset === 0) {
                endDate = new Date(startDate.setDate(startDate.getDate()))
            } else {
                endDate = new Date(startDate.setDate(startDate.getDate() + 1));
            }
            if(endDate.getDay() !== 0 && endDate.getDay() !== 7){
                dateArray.push({
                    date: moment(endDate).format('DD'),
                    day: moment(endDate).format('dddd'),
                    check: false,
                });
                offset++;
            }
        }
        this.setState({dateArray});
    }

    render() {
        return (
            <View style={styles.container}>

                <MyStatusBar backgroundColor="#D64B12" barStyle="light-content" />

                {AppLoader.renderLoading(this.state.loader)}

                <SafeAreaView>
                    <AddAddressHeader
                        text={'Horario de entrega'}
                        isImage={true}
                        onLeftIcon={this._onLeftIconPress.bind(this)}
                    />
                </SafeAreaView>

                <View style={styles.container}>
                    <View style={{flexDirection: 'row',height: hp('50%'), width: wp('100%')}}>
                        <View style={{flexDirection: 'column', width: wp('30%')}}>
                            <FlatList
                                data={this.state.dateArray}
                                // ItemSeparatorComponent={() => <View style={styles.separator} />}
                                renderItem={({ item, index }) => (
                                    <TouchableOpacity
                                        onPress={() => this._selectDate(item)}
                                        style={[styles.timePicker, {borderLeftColor: item.check?'#D64B12':'#ffffff'}]}>
                                        <Text style={{opacity: .6, left: 3}}>{item.day}</Text>
                                        <Text style={{left: 3}}>{item.date}{' '}{this.state.currentMonth}</Text>
                                    </TouchableOpacity>
                                )}
                                keyExtractor={(item, index) => `message ${index}`}
                            />
                        </View>
                        <View style={{ alignItems: 'center',flexDirection: 'column', width: wp('70%')}}>

                            <FlatList
                                data={this.state.timeSchedule}
                                renderItem={({ item, index }) => (
                                    <TouchableOpacity
                                        onPress={() => this._onSelectTime(item)}
                                        style={styles.timing}>
                                        <Text>{item.time}</Text>
                                        <Text style={{color: '#6c9f27'}}>L. {item.price}</Text>
                                    </TouchableOpacity>
                                )}
                                keyExtractor={(item, index) => `message ${index}`}
                            />
                        </View>
                    </View>

                </View>

            </View>
        );
    }

    _selectDate = (date) => {
        let { dateArray } = this.state;
        dateArray.map((item) => {
            if(item.date === date.date) {
                item.check = !item.check;
            } else {
                item.check = false;
            }
        });
        this.setState({dateArray});
    };

    _onSelectTime = (item) => {
        this.setState({loader: true});
        Service._updateTimeSchdule(item, (resp) => {
            if(!resp.error) {
                this._updateUser(item);
            } else {
                this.setState({loader: false}, () => {
                    setTimeout(() => {
                        alert(resp.message);
                    }, 500)
                })
            }
        })
    };

    _updateUser = (timeArray) => {
        let user =  this.instance.getUser();
        user.timeSchedule = timeArray;
        this.instance.setUser(user);
        this.setState({loader: false}, () => {
            setTimeout(() => {
                this.props.navigation.goBack()
            }, 500);
        });
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f7faf8'
    },
    statusBar: {
        height: STATUSBAR_HEIGHT,
    },
    appBar: {
        backgroundColor:'#79B45D',
        height: APPBAR_HEIGHT,
    },
    content: {
        flex: 1,
        backgroundColor: '#33373B',
    },
    timePicker: {
        borderLeftWidth: 4,
        alignItems: 'flex-start',
        marginLeft: wp('6%'),
        marginTop: wp('4%')
    },
    timing: {
        flexDirection: 'row',
        width: wp('50%'),
        marginTop: wp('6%'),
        justifyContent: 'space-between'
    }
});
