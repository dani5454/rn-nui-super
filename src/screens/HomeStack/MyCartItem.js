import React from 'react';
import {Platform, SafeAreaView, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import { NavigationEvents } from 'react-navigation';
import CommonDataManager from '../../service/Singlenton';
import HeaderWithBack from '../../components/HeaderWithBack';
import {hp, wp} from './../../UtilMethods/Utils';
import Gesture from './../../components/SwipeOutGuesture';
import service from '../../firebase/Firebase';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? hp('6%') : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

const MyStatusBar = ({backgroundColor, ...props}) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
        <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
);

export default class MyCartItem extends React.Component {
    instance = CommonDataManager.getInstance();

    constructor(props) {
        super(props);


        this.state = {
            marketName: this.props.navigation.state.params.marketName,
            myCartItem: this.props.navigation.state.params.myCartItem,
            colonial: [],
            la_Colonia: [],
            price_Smart: [],
            price: 0,
            emptyCart: false,
            moveToDetail: false,
            didUpdate: false,
            min_price: 0,
            minMarketName: '',
        };
    }

    _divideCartItem = (myCartItem, sum) => {
        let markets = [
            'Colonial',
            'La Colonia',
            'Price Smart',
        ];
        let col = [], laCol=[], smart=[];
        let show = false;
        let marketName = '';
        let price = 0;
        myCartItem.map((item) => {
            if(item.shop === markets[0]) {
                if(this._getMyMarketSum('Colonial') > parseInt(item.min_limit)) {
                    show = true;
                } else {
                    show = false;
                    marketName = markets[0];
                    price = item.min_limit;
                }
                col.push(item);
            } else if(item.shop === markets[1]){
                if(this._getMyMarketSum('La Colonia')  > parseInt(item.min_limit)) {
                    show = true;
                } else {
                    show = false;
                    marketName = markets[1];
                    price = item.min_limit;
                }
                laCol.push(item);
            } else if(item.shop === markets[2]){
                if(this._getMyMarketSum('Price Smart')  > parseInt(item.min_limit)) {
                    show = true;
                } else {
                    show = false;
                    marketName = markets[2];
                    price = item.min_limit;
                }
                smart.push(item);
            }
        });

        this.setState({colonial: col,
            la_Colonia: laCol,
            moveToDetail: show,
            price_Smart: smart,
            min_price: price,
            minMarketName: marketName
        });
    };

    _getMyMarketSum = (marketName) => {

        const { myCartItem } = this.state;

        let la_Colonia  = myCartItem.filter(find => find.shop === 'Colonial');
        let price_Smart  = myCartItem.filter(find => find.shop === 'Price Smart');
        let colonial  = myCartItem.filter(find => find.shop === 'La Colonia');

        let sum = 0;
        if(la_Colonia.length > 0 && la_Colonia[0].shop === marketName) {
            la_Colonia.forEach((value, index) =>{
                sum += parseInt(value.price) * value.quanity;
            });
            return parseInt(sum);
        } else if (price_Smart.length > 0 && price_Smart[0].shop === marketName) {
            price_Smart.forEach((value, index) =>{
                sum += parseInt(value.price) * value.quanity;
            });
            return parseInt(sum);
        } else if (colonial.length > 0 && colonial[0].shop === marketName) {
            colonial.forEach((value, index) =>{
                sum += parseInt(value.price) * value.quanity;
            });
            return parseInt(sum);
        }
    };

    _onLeftIconPress = () => {
      this.props.navigation.goBack();
    };

    _onPress = (item, click) => {

        if(click === 'minus') {

            this._decrementToCart(item).done()

        } else if (click === 'add') {

            this._addToCart(item).done();

        } else if (click === 'edit') {

            // edit

        } else {

            this._deleteCart(item).done()

        }
    };

    _onWillFocus = (payload) => {
        if(payload.state.params.message === 'pago detectado con éxito') {
            setTimeout(() => {
                // this.setState({emptyCart: true});
                alert('pago detectado con éxito');
            }, 500);
        }
        this.setState({
            marketName: payload.state.params.marketName,
            myCartItem: payload.state.params.myCartItem,
        });
        if(payload.state.params.myCartItem.length === 0) {
            this.setState({emptyCart: true});
        }
        let sum = 0;
        payload.state.params.myCartItem.forEach((value, index) => {
            sum += parseInt(value.price) * value.quanity;
        });
        this.setState({price: sum});
        this._divideCartItem(payload.state.params.myCartItem, sum);
    };

    render() {

        return(
            <View style={{flex: 1}}>
                <MyStatusBar backgroundColor="#D64B12" barStyle="light-content" />

                <NavigationEvents onWillFocus={payload => this._onWillFocus(payload)} />
                <View style={{flex: 1}}>
                    <HeaderWithBack
                        text={'Carrito'}
                        icon={require('./../../assets/icon/ic_menu_dots.png')}
                        onLeftIcon={this._onLeftIconPress.bind(this)}
                    />
                    <View style={{flex: 1}}>
                        {/*{myCartItem.length > 0 && (*/}
                        {/*    <Gesture card={myCartItem} marketName={this.state.marketName} onPress={this._onPress.bind(this)} />*/}
                        {/*)}*/}
                        <View style={{height: hp('77%')}}>
                            <ScrollView>
                                {this.state.colonial.length > 0 && (
                                    <Gesture card={this.state.colonial} marketName={this.state.colonial[0].shop} onPress={this._onPress.bind(this)} />
                                )}
                                {this.state.la_Colonia.length > 0 && (
                                    <Gesture card={this.state.la_Colonia} marketName={this.state.la_Colonia[0].shop} onPress={this._onPress.bind(this)} />
                                )}
                                {this.state.price_Smart.length > 0 && (
                                    <Gesture card={this.state.price_Smart} marketName={this.state.price_Smart[0].shop} onPress={this._onPress.bind(this)} />
                                )}
                            </ScrollView>
                        </View>

                        <View style={{flex: 1,flexDirection: 'column', justifyContent: 'flex-end'}}>
                            {this._renderFooter()}
                        </View>
                    </View>
                </View>
            </View>
        );
    }

    _renderFooter = () => {
        return(
            <TouchableOpacity
                activeOpacity={1}
                onPress={() => this._moveToDetailInfo()}
                style={{flex: 1, justifyContent: 'flex-end'}}>
                <View style={{backgroundColor: this.state.moveToDetail?'#6c9f27':'#c2c0c0'}}>
                    <SafeAreaView>
                        <View style={{height: hp('6%'),flexDirection: 'column',justifyContent: 'center', alignItems: 'center'}}>
                            {!this.state.moveToDetail && (
                                <Text style={{color: '#ffffff', fontSize: 18}}>
                                    {this.state.emptyCart?'Tu carrito esta vacío':'Pedido mínimo '+this.state.minMarketName+':'+'L.'+this.state.min_price}
                                </Text>
                            )}
                            <View style={{width: wp('100%'),justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row'}}>
                                <View/>
                                <Text style={{color: '#ffffff',left:wp('6%'), fontSize: 18}}>Comprar</Text>
                                <View style={{right:wp('3%')}}>
                                    <Text style={{color: '#ffffff', fontSize: 18}}>L. {this.state.price}</Text>
                                </View>
                            </View>
                        </View>
                    </SafeAreaView>
                </View>
            </TouchableOpacity>
        );
    };

    _moveToDetailInfo = () => {
        const { moveToDetail } = this.state;
        if(moveToDetail) {
            this.props.navigation.navigate('Detail', {price: this.state.price, marketName: this.state.marketName});
        }
    };

    _addToCart = async (item) => {
        let arr = [];let newArray = [];
        let userCart = this.instance.getUserCart();
        userCart.map(async (cart, index) => {
            if(item.name === cart.name && item.category_type === cart.category_type && item.shop === cart.shop) {
                if(item.shop === cart.shop) {
                    item.quanity = cart.quanity + 1;
                    arr.push(item);
                    userCart.splice(index,1);
                }
            }
        });
        newArray = [...arr, ...userCart];
        this.instance.setUserCart(newArray);
        this._checkCart(newArray);
        await service._addToCart('123',newArray, (resp) => {
            if(!resp.error) {
                let sum  = 0;
                newArray.forEach((value, index) =>{
                    sum += parseInt(value.price) * value.quanity;
                });
                this.setState({price: sum, myCartItem: newArray});
                this._divideCartItem(newArray, sum);
            }
        });
    };

    _decrementToCart = async (item) => {
        let arr = [];let newArray = [];
        let oneItem = false;
        let userCart = this.instance.getUserCart();
        userCart.map(async (cart, index) => {
            if(item.name === cart.name && item.category_type === cart.category_type && cart.shop === item.shop) {
                if(cart.quanity === 1) {
                    userCart.splice(index,1);
                    oneItem = true;
                } else {
                    item.quanity = cart.quanity - 1;
                    arr.push(item);
                    userCart.splice(index,1);
                }
            }
        });
        if(oneItem) {
            newArray = [...userCart];
        } else {
            newArray = [...arr, ...userCart];
        }
        this.instance.setUserCart(newArray);
        this._checkCart(newArray);
        await service._addToCart('123',newArray, (resp) => {
            if(!resp.error) {
                let sum  = 0;
                newArray.forEach((value, index) =>{
                    sum += parseInt(value.price) * value.quanity;
                });
                this.setState({price: sum, myCartItem: newArray});
                this._divideCartItem(newArray, sum);
            }
        });
    };

    _deleteCart = async (item) => {
        let newArray = [];
        let userCart = this.instance.getUserCart();
        userCart.map(async (cart, index) => {
            if(item.name === cart.name && item.category_type === cart.category_type) {
                if(item.shop === cart.shop) {
                    userCart.splice(index,1);
                }
            }
        });
        newArray = [...userCart];

        this.instance.setUserCart(newArray);
        this._checkCart(newArray);
        await service._addToCart('123',newArray, (resp) => {
            if(!resp.error) {
                let sum  = 0;
                newArray.forEach((value, index) =>{
                    sum += parseInt(value.price) * value.quanity;
                });
                this.setState({price: sum, myCartItem: newArray});
                this._divideCartItem(newArray, sum);
            }
        });
    };

    _checkCart = (cart) => {
        if(cart.length === 0) {
            this.setState({emptyCart: true});
        }
    }
}

const styles = StyleSheet.create({
    statusBar: {
        height: STATUSBAR_HEIGHT,
    },
    appBar: {
        backgroundColor:'#79B45D',
        height: APPBAR_HEIGHT,
    },
    content: {
        flex: 1,
        backgroundColor: '#33373B',
    },
    comprar: {
        height: hp('6%'),
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection:'row',
        backgroundColor: '#6c9f27'
    }
});
