import React from 'react';
import {FlatList, Image, SafeAreaView, ScrollView,StatusBar,StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {NavigationEvents} from 'react-navigation';
import HomeHeader from './../../components/HomeHeader';
import ProductList from './../../components/ProductList';
import CommonDataManager from '../../service/Singlenton';
import service from './../../firebase/Firebase';
import {hp, wp} from './../../UtilMethods/Utils';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? hp('6%') : StatusBar.currentHeight;

const MyStatusBar = ({backgroundColor, ...props}) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
        <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
);

export default class SuperMarket extends React.Component {
    instance = CommonDataManager.getInstance();

    constructor(props) {
        super(props);


        this.state = {
            productName: this.props.navigation.state.params !== undefined ? this.props.navigation.state.params.productName : 'Colonial',
            productList: [],
            filteredList: [],
            showCartItem: false,
            cartItemLength: 0,
            refresh: false,
            myCartItem: [],
            userCartList: []
        }
    }

    _onRightIconPress = () => {
        this.props.navigation.navigate('MyCartItem', {marketName: this.state.productName, myCartItem: this.state.myCartItem, message: 'no message'});
    };

    _handleSearchInput = (e) => {
        let text = e.toLowerCase().replace(/[^a-zA-Z ]/g, "");
        let fullList = this.state.filteredList;

        let filtered  = fullList.map((item)=> {
            let val = item.find(find => {
               if (find.name.toLowerCase().match(text)) {
                   return find;
               }
           });
            if(val) {
                return val;
            }
        });
        let data = filtered.filter(function( element ) {
            return element !== undefined;
        });
        let arr = [];
        arr.push(data);

        if(data.length > 0) {
            if (!text || text === '') {
                this.setState({productList: fullList, search: ''});
            } else if (filtered) {
                this.setState({productList: arr, search: text});
            }
        } else {
            this.setState({productList: [], search: ''});
        }
    };

    _toggleDrawer = () => {
        this.props.navigation.openDrawer();
    };

    getProducts = (productName, array, userCart) => {
        service._getProductList(productName, (resp) => {
            if(!resp.error) {
                if(resp.data.size > 0) {
                    let pro = resp.data._docs[0].data().productsList;
                    for(let i=0; i<pro.length; i++) {
                        if(Object.entries(pro[i].products[0]).length !== 0) {
                            array.push(pro[i].products);
                        }
                    }
                }
                this.setState({
                    productList: array,
                    filteredList: array,
                    userCartList: userCart
                });
            }
        });
    };

    _onWillFocus = (payload) => {
        let array = [];
        // let arr = [
        //     'Frutas_y_verduras',
        //     'Lacteos',
        //     'Carnes_y_mariscos',
        //     'Panaderia_y_reposteria',
        // ];
        let quantity = 0;
        let cartItem = this.instance.getUserCart();
        if(cartItem.length > 0) {
            cartItem.forEach((value, index) =>{
                quantity += parseInt(value.quanity);
            });
            this.setState({showCartItem: true,
                cartItemLength: quantity, myCartItem: cartItem});
        }
        if(typeof payload.state.params !== 'undefined') {
            this.setState({productName: payload.state.params.productName});

            this.getProducts(payload.state.params.productName,array,cartItem);
        } else {
            this.setState({productName: 'Colonial'});

            this.getProducts('Colonial',array, []);
        }
    };

    _onCardPress = (item) => {
        item.shop = this.state.productName;
        this.props.navigation.navigate('CardDetail', { marketName: this.state.productName,card: item, cartItemLength: this.state.cartItemLength});
    };

    _onWillBlur = () => {
      this.setState({productList: [],
          filteredList: [],showCartItem: false,myCartItem: []})
    };

    render() {
        return(
            <View style={{flex: 1, backgroundColor: '#f2efe9'}}>
                <MyStatusBar backgroundColor="#D64B12" barStyle="light-content" />
                <SafeAreaView style={{flex: 1}}>
                    <HomeHeader
                        onShowItem={this.state.showCartItem}
                        cartItem={this.state.cartItemLength}
                        onRightIcon={this._onRightIconPress.bind(this)}
                        onChangeText={this._handleSearchInput.bind(this)}
                    />
                    <NavigationEvents
                        onDidFocus={payload => this._onWillFocus(payload)}
                        onWillBlur={payload => this._onWillBlur(payload)}
                    />
                    <View style={{flex: 1}}>
                        <View style={{justifyContent: 'space-between',height: hp('8%'),flexDirection: 'row',alignItems: 'center', backgroundColor: '#6c9f27'}}>
                            <TouchableOpacity
                                onPress={() => this._toggleDrawer()}
                                style={{marginLeft: wp('4%'), flexDirection:'row', alignItems: 'center'}}>
                                <Text style={{color: 'white'}}>Categorias</Text>
                                <Image source={require('./../../assets/icon/ic_arrow_down.png')}
                                       style={{tintColor: 'white',marginLeft: 8,width: wp('5%'),marginTop: 5, height: wp('5%')}}
                                />
                            </TouchableOpacity>
                            <View style={{marginRight: wp('4%')}}>
                                <Text style={{color: 'white'}}>{this.state.productName}</Text>
                            </View>
                        </View>
                        <ScrollView>
                            <View style={{flex: 1}}>
                                {this.state.productList.length > 0 && (
                                    <FlatList
                                        data={this.state.productList}
                                        renderItem={({ item, index }) => {
                                            return(
                                                <ProductList color={'#f2efe9'}
                                                             userCartList={this.state.userCartList}
                                                             shop={this.state.productName}
                                                             onVerTodo={this._onPress.bind(this)}
                                                             addToCart={this._addToCart.bind(this)}
                                                             decrementProduct={this._decrementToCart.bind(this)}
                                                             productList={item}
                                                             onCardPress={this._onCardPress.bind(this)}
                                                />
                                            );
                                        }}
                                    />
                                )}
                            </View>
                        </ScrollView>
                    </View>
                </SafeAreaView>
            </View>
        );
    }

    _addToCart = async (count, item) => {
        let arr = [];let newArray = [];

        let userCart = this.instance.getUserCart();

        if(userCart.length > 0) {
            userCart.map(async (cart, index) => {
                if(item.name === cart.name && item.category_type === cart.category_type) {
                    if(this.state.productName.toString() === cart.shop) {
                        item.quanity = cart.quanity + 1;
                        arr.push(item);
                        userCart.splice(index,1);
                    }
                }
            });
            if(arr.length === 0) {
                item.quanity = count;
                arr.push(item);
            }
            item.shop = this.state.productName;
            newArray = [...arr, ...userCart];
            this.instance.setUserCart(newArray);
            this.setState({myCartItem: newArray});
            await service._addToCart('123',newArray, (resp) => {
                if(!resp.error) {
                    let cartCount = 0;
                    newArray.forEach((value) =>{
                        cartCount += parseInt(value.quanity);
                    });
                    this.setState({showCartItem: true,
                        cartItemLength: cartCount})
                }
            });
        } else {
            this.onFirstAdd(item,arr,count).done();
        }
        // service._getUserCartList(async (userCart) => {
        //     if(!userCart.error) {
        //         if(typeof userCart.data !== "undefined") {
        //             if(userCart.data.UserCartArray.length > 0) {
        //                 userCart.data.UserCartArray.map(async (cart, index) => {
        //                     if(item.name === cart.name && item.category_type === cart.category_type) {
        //                         if(this.state.productName.toString() === cart.shop) {
        //                             item.quanity = cart.quanity + 1;
        //                             arr.push(item);
        //                             userCart.data.UserCartArray.splice(index,1);
        //                         }
        //                     }
        //                 });
        //                 if(arr.length === 0) {
        //                     item.quanity = count;
        //                     arr.push(item);
        //                 }
        //                 item.shop = this.state.productName;
        //                 newArray = [...arr, ...userCart.data.UserCartArray];
        //                 this.setState({myCartItem: newArray});
        //                 await service._addToCart('123',newArray, (resp) => {
        //                     if(!resp.error) {
        //                         this.setState({showCartItem: true,
        //                             cartItemLength: quantity})
        //                     }
        //                 });
        //             } else {
        //                 this.onFirstAdd(item,arr,count).done();
        //             }
        //         } else {
        //             this.onFirstAdd(item,arr,count).done();
        //         }
        //     }
        // });
    };

    onFirstAdd = async (item, arr, count) => {
        item.quanity = count;
        item.shop = this.state.productName;
        arr.push(item);
        this.setState({myCartItem: arr});
        this.instance.setUserCart(arr);
        await service._addToCart('123',arr, (resp) => {
            if(!resp.error) {
                this.setState({showCartItem: true,
                    cartItemLength: count})
            }
        });
    };

    _decrementToCart = async (count, item) => {
        let arr = [];let newArray = [];
        let userCart = this.instance.getUserCart();
        if(count > 0) {
            userCart.map(async (cart, index) => {
                if(item.name === cart.name && item.category_type === cart.category_type && cart.shop === this.state.productName) {
                    item.quanity = cart.quanity - 1;
                    arr.push(item);
                    userCart.splice(index,1);
                }
            });
            item.shop = this.state.productName;
            newArray = [...arr, ...userCart];
            this.instance.setUserCart(newArray);
            this.setState({myCartItem: newArray});
            await service._addToCart('123',newArray, (resp) => {
                console.log('Resp-Decrement---->>>>', resp);
                if(!resp.error) {
                    let cartCount = 0;
                    newArray.forEach((value) =>{
                        cartCount += parseInt(value.quanity);
                    });
                    this.setState({showCartItem: true,
                        cartItemLength: cartCount, userCartList: newArray});
                }
            });
        } else {
            userCart.map(async (cart, index) => {
                if(item.name === cart.name && item.category_type === cart.category_type) {
                    userCart.splice(index,1);
                }
            });
            item.shop = this.state.productName;
            newArray = [...userCart];
            this.instance.setUserCart(newArray);
            this.setState({myCartItem: newArray});
            await service._addToCart('123',newArray, (resp) => {
                console.log('Resp-Decrement---->>>>', resp);
                if(!resp.error) {
                    let cartCount = 0;
                    newArray.forEach((value) =>{
                        cartCount += parseInt(value.quanity);
                    });
                    this.setState({showCartItem: true,
                        cartItemLength: cartCount, userCartList: newArray})
                }
            });
        }

        // if (count > 0) {
        //     service._getUserCartList(async (userCart) => {
        //         userCart.data.UserCartArray.map(async (cart, index) => {
        //             if(item.name === cart.name && item.category_type === cart.category_type) {
        //                 item.quanity = item.quanity - 1;
        //                 arr.push(item);
        //                 userCart.data.UserCartArray.splice(index,1);
        //             }
        //         });
        //         newArray = [...arr, ...userCart.data.UserCartArray];
        //         this.setState({myCartItem: newArray});
        //         await service._addToCart('123',newArray, (resp) => {
        //             console.log('Resp-Decrement---->>>>', resp);
        //             if(!resp.error) {
        //                 // this.setState({showCartItem: true,
        //                 //     cartItemLength: quantity})
        //             }
        //         });
        //     })
        // } else {
        //     service._getUserCartList(async (userCart) => {
        //         userCart.data.UserCartArray.map(async (cart, index) => {
        //             if(item.name === cart.name && item.category_type === cart.category_type) {
        //                 userCart.data.UserCartArray.splice(index,1);
        //             }
        //         });
        //         newArray = [...userCart.data.UserCartArray];
        //         this.setState({myCartItem: newArray});
        //         await service._addToCart('123',newArray, (resp) => {
        //             console.log('Resp-Decrement---->>>>', resp);
        //             if(!resp.error) {
        //                 // this.setState({showCartItem: true,
        //                 //     cartItemLength: quantity})
        //             }
        //         });
        //     })
        // }
    };

    _onPress = (item, catName) => {
        this.props.navigation.navigate('VerTodoList', {product: item, catName: catName,productName: this.state.productName});
    }
}

const styles = StyleSheet.create({
    statusBar: {
        height: STATUSBAR_HEIGHT,
    },
    content: {
        flex: 1,
        backgroundColor: '#33373B',
    },
});
