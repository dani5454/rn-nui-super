import React, { Component } from 'react';
import {
    View,
    PermissionsAndroid,
    Text,
    Image,
    StyleSheet,
    Dimensions,
    Platform,
    TouchableOpacity
} from 'react-native';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import Geocoder from 'react-native-geocoding';
import Geolocation from 'react-native-geolocation-service';
import AsyncStorage from '@react-native-community/async-storage';
import {wp, hp} from '../../UtilMethods/Utils';

const {width, height} = Dimensions.get('window');
Geocoder.init("AIzaSyDpzJA3pGJRA4Tt9oCX3bnDw3VOOqoBcHw");

const ASPECT_RATIO = width / height;
const LATITUDE = 37.774625716497;
const LONGITUDE = -122.41416553035378;
const LATITUDE_DELTA = 0.1222;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

export default class Maps extends Component {
    constructor(props) {
        super(props);

        this.state = {
            latitude: LATITUDE,
            longitude: LONGITUDE,
            address: '',
            country: '',
            city: '',
            state: '',
            street: '',
        };
    }
    componentDidMount() {
        if (Platform.OS === 'android') {
            PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                {
                    'title': 'Permiso de ubicación',
                    'message': 'Nui Super quiere saber tu ubicación',
                },
            ).then((granted) => {
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    this._getLocation();
                } else {
                    console.log('permission not granted');
                }
            })
                .catch((err) => {
                    console.log('--------err--------');
                    console.log(err);
                });
        } else {
            this._getLocation();
        }
    }

    _getLocation = () => {
        Geolocation.getCurrentPosition(
            (position) => {
                // console.log('POSITIONS----->>>>>', position);
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                });
            },
            (error) => {
                // See error code charts below.
                console.log(error.code, error.message);
            },
            { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
        );
    };

    navigateToHome() {
        const { address, city, state, street, country } = this.state;
        let arr = {
            address: address,
            latitude: this.state.latitude,
            longitude: this.state.longitude,
            country: country,
            city: city,
            state: state,
            street: street,
        };
        AsyncStorage.setItem('@saveUserAddress', JSON.stringify(arr));
        this.props.navigation.navigate('AddAddress');
    }

    onRegionChangeCompleted = (region) => {
        let NY = {
            lat: region.latitude,
            lng: region.longitude
        };
        Geocoder.from(NY).then(res => {
            // console.log('formattedAddress---->>>', res);
            let formatAddress = res.results[0].formatted_address;
            this.setState({
                address: formatAddress,
                latitude: res.results[0].geometry.location.lat,
                longitude: res.results[0].geometry.location.lng,
                city: res.results[0].locality,
                country: res.results[0].country,
                state: res.results[0].adminArea,
                street: res.results[0].streetName
            });
        }).catch(err => {
            console.log('------ERROR------');
            console.log(err);
        })
    };

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.navigationBar}>
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.goBack();
                        }}
                        style={styles.buttonBack}
                    >
                        <Image resizeMode="contain" style={{tintColor: 'white',height: wp('6%'),width:wp('6%')}}
                               source={require('./../../assets/icon/ic_back.png')}/>
                    </TouchableOpacity>
                    <Text style={styles.navbarTitleText}>Dirección</Text>
                </View>
                <MapView
                    provider={PROVIDER_GOOGLE}
                    initialRegion={{
                        latitude: this.state.latitude,
                        longitude: this.state.longitude,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA
                    }}
                    style={styles.map}
                    onRegionChangeComplete={this.onRegionChangeCompleted}/>
                <View style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                    <Image
                        source={require('../../assets/icon/ic_Bitmap.png')}
                        style={{ height: hp('10%')}}
                        resizeMode='contain'
                    />
                </View>
                <View style={[styles.bubble, styles.latlng]}>
                    <Text style={styles.centeredText}>
                        {this.state.address}
                    </Text>
                </View>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity style={styles.createProfileButton}
                                      onPress={() => this.navigateToHome()}>
                        <Text style={styles.confirmText}>CONFIRMAR</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    bubble: {
        backgroundColor: 'rgba(255,255,255,0.7)',
        paddingHorizontal: 18,
        paddingVertical: 12,
        borderRadius: 20,
    },
    latlng: {
        width: wp('90%'),
        alignItems: 'stretch',
    },
    button: {
        width: 100,
        paddingHorizontal: 8,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 5,
    },
    buttonContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 20,
        backgroundColor: 'transparent',
    },
    buttonText: {
        textAlign: 'center',
    },
    centeredText: {
        textAlign: 'center',
        color: '#000',
        fontSize: wp('3%'),
        ...Platform.select({
            ios: {
                // fontFamily: 'BackIssuesBB',
                fontWeight: '300'
            },
            android: {
                // fontFamily: 'BackIssuesBB_reg'
            }
        })
    },
    confirmBtn: {
        alignItems: 'center',
        justifyContent: 'center',
        height: hp('5%'),
        width: wp('45%'),
        backgroundColor: '#000',
        borderRadius: 10
    },
    confirmText: {
        color: '#fff',
        ...Platform.select({
            ios: {
                // fontFamily: 'BackIssuesBB',
                fontWeight: '300'
            },
            android: {
                // fontFamily: 'BackIssuesBB_reg'
            }
        })
    },
    navigationBar: {
        height: hp('14%'),
        width: wp('100%'),
        position: 'absolute',
        top: 0,
        zIndex: 1000,
        backgroundColor: 'rgba(0,0,0,0.6))',
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonBack: {
        position: 'absolute',
        top: hp('7%'),
        left: wp('5%'),
        zIndex: 1111
    },
    navbarTitleText: {
        fontSize: wp('8%'),
        color: '#fff',
        opacity: 1.5,
        position: 'absolute',
        top: hp('7%'),
        textShadowColor: '#000',
        textShadowOffset: { width: 3, height: 3 },
        textShadowRadius: 1
    },
    createProfileButton: {
        backgroundColor: '#000',
        width: wp('48%'),
        height: hp('5.5%'),
        borderRadius: wp('2%'),
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5
    },
});
