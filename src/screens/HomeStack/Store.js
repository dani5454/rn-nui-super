import React from 'react';
import {View, Text, Image, TouchableWithoutFeedback, FlatList, ImageBackground, StyleSheet} from 'react-native';
import AppLoader from '../../components/AppLoader';
import Header from './../../components/Header';
import service from './../../firebase/Firebase';
import {hp, wp} from '../../UtilMethods/Utils';

let MARKET_LENGTH = -1;

export default class Store extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            marketsArr: [],
            loading: true
        };
    }

    componentDidMount() {
        service.fetchMarkets((resp) => {
            if(!resp.error) {
                // Apply recursion to avoid loops
                this.fetchMarketProfile(resp.data.docs);
            } else {
                this.setState({loading: false, marketsArr: []});
            }
        });
    }

    fetchMarketProfile = (markets) => {
        MARKET_LENGTH++;
        if(MARKET_LENGTH < markets.length) {
            service._getUserProfile(markets[MARKET_LENGTH].id, (market) => {
                if (!market.error) {
                    this.state.marketsArr.push(market.data);
                    this.fetchMarketProfile(markets);
                } else {
                    this.setState({loading: false, marketsArr: []});
                }
            });
        } else {
            this.finishSyncMarkets(this.state.marketsArr)
        }
    };

    finishSyncMarkets = (marketsArr) => {
        this.setState({loading:false,marketsArr});
    };

    _onRightIconPress = () => {};

    onMarketPress = (market) => {
        this.props.navigation.navigate('Budget',
            {
                productName: market.name,
                budget: parseInt(market.minimumPrice)
            });
    };

    renderMarkets = (market) => {
        return(
            <ImageBackground
                source={{uri : market.photo}}
                style={{height:hp('25'),width:null,flex:1,resizeMode:'cover'}}
            >
                <TouchableWithoutFeedback onPress={() => this.onMarketPress(market)}>
                    <View style={styles.container}>
                        <Image source={{uri: market.logo}}
                               style={styles.logo}
                        />
                        <Text style={{color: '#FFF'}}>Pedido min: {market.minimumPrice}</Text>
                    </View>
                </TouchableWithoutFeedback>
            </ImageBackground>
        );
    };

    render() {
        return(
            <View style={{flex: 1}}>
                {AppLoader.renderLoading(this.state.loading)}
                <Header onRightIcon={this._onRightIconPress.bind(this)}
                />
                <FlatList
                    data={this.state.marketsArr}
                    ItemSeparatorComponent={() => <View style={styles.separator}/>}
                    renderItem={({item, index}) => this.renderMarkets(item)}
                    contentContainerStyle={{
                        paddingBottom: 10
                    }}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'flex-end',
        marginBottom: 5
    },
    logo: {
        width: wp('40'),
        height: hp('5'),
        marginBottom: 20,
        resizeMode: 'cover'
    },
    separator: {
        marginTop: 8,
    }
});

// <View style={{flex: 1}}>
//     <View style={{flex: 1,borderTopRightRadius: wp('10%'),
//         borderTopLeftRadius: wp('10%')
//     }}>
//         <ImageBackground
//             source={require('./../../assets/images/Welcome-3.jpg')}
//             resizeMode={'cover'}
//             style={{flex: 1, overflow: 'hidden',
//                 borderTopRightRadius: wp('10%'),borderTopLeftRadius: wp('10%')
//             }} >
//
//             <View style={{alignItems: 'center', marginTop: wp('3%'), flexDirection: 'column'}}>
//                 <Image source={require('./../../assets/icon/ic_arrow_down.png')}
//                        style={{ width: wp('5%'), height: wp('5%')}}
//                        resizeMode={'contain'}
//                 />
//                 <TouchableOpacity
//                     onPress={() => this.props.navigation.navigate('Budget', { productName: 'Colonial', budget: 500})}
//                 >
//                     <Image source={require('./../../assets/images/intro_logo_4.png')}
//                            style={{ width: wp('45%'), height: hp('10%')}}
//                            resizeMode={'contain'}
//                     />
//                 </TouchableOpacity>
//             </View>
//
//             <View style={{
//                 justifyContent: 'flex-end', flex: 1
//             }}>
//                 <View style={{
//                     backgroundColor: '#8cc63e',
//                     height: hp('30%'), borderTopRightRadius: wp('10%'),
//                     borderTopLeftRadius: wp('10%')
//                 }}>
//                     <View style={{alignItems: 'center', marginTop: hp('2%') }}>
//                         <Image source={require('./../../assets/icon/ic_arrow_down.png')}
//                                style={{ width: wp('5%'), height: wp('5%')}}
//                                resizeMode={'contain'}
//                         />
//                         <TouchableOpacity
//                             onPress={() => this.props.navigation.navigate('Budget', { productName: 'La Colonia', budget: 500})}
//                         >
//                             <Image source={require('./../../assets/images/02_00000.png')}
//                                    style={{width: wp('60%'), height: hp('16%'),bottom: wp('7%')}}
//                                    resizeMode={'contain'}
//                             />
//                         </TouchableOpacity>
//                     </View>
//                     <View style={{
//                         flex: 1, height: hp('15%'),
//                         justifyContent: 'flex-end'
//                     }}>
//                         <View style={{
//                             height: hp('15%'),borderTopRightRadius: wp('10%'),
//                             borderTopLeftRadius: wp('10%'), backgroundColor: '#094fa3'
//                         }}>
//                             <View style={{alignItems: 'center', marginTop: hp('2%')}}>
//                                 <Image source={require('./../../assets/icon/ic_arrow_down.png')}
//                                        style={{ width: wp('5%'), height: wp('5%')}}
//                                        resizeMode={'contain'}
//                                 />
//                                 <TouchableOpacity
//                                     onPress={() => this.props.navigation.navigate('Budget', { productName: 'Price Smart', budget: 1000})}
//                                 >
//                                     <Image source={require('./../../assets/images/01_00000.png')}
//                                            style={{ bottom: wp('12%'),width: wp('80%'), height: hp('20%')}}
//                                            resizeMode={'contain'}
//                                     />
//                                 </TouchableOpacity>
//                             </View>
//                         </View>
//
//                     </View>
//                 </View>
//             </View>
//         </ImageBackground>
//     </View>
// </View>
