import React from 'react';
import {View,Text,Dimensions,Image,ImageBackground,TextInput,TouchableOpacity,TouchableHighlight,Modal,StyleSheet} from 'react-native';
import CommonDataManager from '../../service/Singlenton';
import { wp, hp } from './../../UtilMethods/Utils';
import service from './../../firebase/Firebase';

const { width } = Dimensions.get("window");

export default class Budget extends React.Component {
    instance = CommonDataManager.getInstance();

    constructor(props) {
        super(props);

        this.state = {
            budget: '',
            errorMessage: false,
            productName: this.props.navigation.state.params.productName,
            productBudget: this.props.navigation.state.params.budget,
            errorMessaging: 'Presupuesto Minimo',
        }
    }

    componentDidMount() {
        service._getUserCartList(async (userCart) => {
            if(typeof userCart.data !== "undefined") {
                if(userCart.data.UserCartArray.length > 0) {
                    this.instance.setUserCart(userCart.data.UserCartArray);
                }
            }
        });
    }

    _onChangeBudget = (text) => {
        this.setState({ budget: text});
    };

    _onStore = () => {
        const { budget } = this.state;
        if(!isNaN(parseInt(budget)) && budget.length > 0) {
            if (parseInt(budget) < this.state.productBudget) {
                this.setState({ errorMessage: true });
            } else {
                this.props.navigation.navigate('SuperMarket',
                    {
                        productName: this.state.productName,
                        // userCartList: this.state.userCartList
                    });
            }
        } else {
            this.setState({ errorMessage: true });
        }
    };

    render() {
        return(
            <ImageBackground
                source={require('./../../assets/images/budget.jpg')}
                resizeMode={'cover'}
                style={{flex: 1}}
            >
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                    <View style={styles.budgetContainer}>
                        <View>
                            <Text style={{color: '#5c5c5c',fontStyle: 'italic',fontSize: 22,fontWeight: 'bold'}}>
                                ¿Cuál es tu presupuesto?</Text>
                        </View>
                        <View style={styles.inputBox}>
                            <View style={{width: '30%',left: 5, alignItems: 'flex-end',justifyContent: 'center'}}>
                                <Text style={{marginBottom: 2,fontSize: 16}}>L.</Text>
                            </View>
                            <View style={{width: '70%', justifyContent: 'center'}}>
                                <TextInput
                                    style={{fontSize: 16}}
                                    maxLength={6}
                                    keyboardType={'number-pad'}
                                    value={this.state.budget}
                                    onChangeText={budget => this._onChangeBudget(budget)}
                                />
                            </View>
                        </View>
                        <TouchableOpacity
                            onPress={() => this._onStore()}
                            style={{borderRadius: wp('10%'), backgroundColor: '#d64b12',
                                width: wp('40%'), alignItems: 'center',
                                height: hp('5%'), justifyContent: 'center'
                            }}>
                            <Text style={{color: 'white', fontSize: wp('4%')}}>Iniciar compra</Text>
                        </TouchableOpacity>
                    </View>
                    <Modal
                        onRequestClose={() => this.setState({ errorMessage: false })}
                        animationType="fade"
                        transparent={true}
                        visible={this.state.errorMessage}
                    >
                        <TouchableHighlight
                                          onPress={() => this.setState({ errorMessage: false })}
                                          style={styles.modalRootStyle}>
                            <View style={styles.modalBoxStyle}>
                                <View style={{flex: 1}} >
                                    <View style={{alignItems: 'center', zIndex: 1}}>
                                        <View style={{
                                            top: wp('15%'),
                                            width: 50,
                                            height: 50, borderRadius: 50, zIndex: 1, alignItems: 'center',
                                            justifyContent: 'center', backgroundColor: '#FF6F00'
                                        }}>
                                            <Image source={require('./../../assets/images/notification_logo.png')}
                                                   style={{
                                                       width: wp('12%'), height: wp('12%')
                                                   }}
                                            />
                                        </View>
                                    </View>
                                    <ImageBackground
                                        source={require('./../../assets/images/notification.png')}
                                        resizeMode={'contain'}
                                        style={{flex: 1}}
                                    >
                                        <View style={{marginTop: wp('8%'),flex: 1,alignItems: 'center', justifyContent: 'center'}}>
                                            <View>
                                                <Text>{this.state.errorMessaging}</Text>
                                                <View style={{flexDirection: 'row'}}>
                                                    <Text>{'es '}</Text>
                                                    <Text style={{fontWeight: 'bold'}}>{'L.'}{this.state.productBudget}</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </ImageBackground>
                                </View>
                            </View>
                        </TouchableHighlight>
                    </Modal>
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    modalRootStyle: {
        flex: 1,
        backgroundColor: "rgba(52, 52, 52, 0.8)",
        alignItems: 'center',
        justifyContent: "center",
    },
    modalBoxStyle: {
        width: width - 60,
        height: hp('30%'),
        borderRadius: 5,
    },
    budgetContainer: {
        width: wp('100%'),
        marginBottom: hp('7%'),
        justifyContent: 'space-between',
        flexDirection: 'column',
        height: hp('20%'),
        alignItems: 'center'
    },
    inputBox: {
        backgroundColor: 'white',
        width: wp('70%'),
        height: hp('6%'),
        flexDirection: 'row',
        justifyContent: 'center',
        borderTopLeftRadius: wp('2.5%'),
        borderTopRightRadius: wp('2.5%'),
        borderBottomLeftRadius: wp('2.5%'),
        borderBottomRightRadius: wp('2.5%')
    }
});
