import React from 'react';
import { View, ActivityIndicator, Text, StyleSheet } from 'react-native';
import axios from 'axios';
import qs from 'qs';
import Service from '../../firebase/Firebase';
import { WebView } from 'react-native-webview';
import CommonDataManager from '../../service/Singlenton';

export default class Paypal extends React.Component {
    instance = CommonDataManager.getInstance();

    constructor(props) {
        super(props);

        this.state = {
            accessToken: null,
            approvalUrl: null,
            paymentId: null,
            access_token: '',
            isLoading: false,
            price: this.props.navigation.state.params.price,
            marketName: this.props.navigation.state.params.marketName,
        }
    }

    componentDidMount() {
        const { price } = this.state;
        const create_payment_json = JSON.stringify({
            intent: "sale",
            payer: {
                payment_method: "paypal"
            },
            redirect_urls: {
                return_url: "https://example.com",
                cancel_url: "https://example.com"
            },
            transactions: [
                {
                    item_list: {
                        items: [
                            {
                                name: "item",
                                sku: "item",
                                price: `${price}`,
                                currency: "USD",
                                quantity: 1
                            }
                        ]
                    },
                    amount: {
                        currency: "USD",
                        total: `${price}`
                    },
                    description: "This is the payment description."
                }
            ]
        });

        this.getRequest().then((res) => {
            if(res.status === 200) {
                this.setState({access_token: res.data.access_token});
                this.payment(create_payment_json, res.data.access_token).then((resp) => {
                    const { id, links } = resp.data;
                    const approvalUrl = links.find(data => data.rel == "approval_url");
                    this.setState({
                        paymentId: id,
                        approvalUrl: approvalUrl.href
                    })
                }).catch((error) => {
                    console.log(error);
                });

            }
        }).catch((error) => {
            console.log(error);
        });

    }

    getRequest = () => {
        return axios({
            method: 'post',
            url: 'https://api.sandbox.paypal.com/v1/oauth2/token',
            headers: {
                Authorization: 'Basic QVNCMlR0bVhSbVFXTFVhZDQySURKaUdGY2JGMS0zOXNLOHduY3NiS2szaDJjS1F5bTA2MDh5SXNYTXI1VVNvS1dIYkJkTjNUUk01dld0Z0E6RVBlQ3I3S3NMdW92XzlaZDN2YzhaUGVHN2ZlY21yc1lHYkJZSFJrYWZxUXpObHhNY2JRamtCejB2N2tQaTNNTEdWUWlLQjRUSFViZ01UTkU=',
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            data: qs.stringify({
                grant_type: 'client_credentials',
            }),
        });
    };

    payment = (data, accessToken) => {
        return axios({
            method: 'post',
            url: 'https://api.sandbox.paypal.com/v1/payments/payment',
            headers: {
                Authorization: `Bearer ${accessToken}`,
                'Content-Type': 'application/json',
            },
            data: data,
        });

    };

    paymemtExecute = (paymentId, PayerID) => {
        const data = JSON.stringify({
            payer_id: PayerID,
        });

        return axios({
            method: 'post',
            url: `https://api.sandbox.paypal.com/v1/payments/payment/${paymentId}/execute`,
            headers: {
                Authorization: `Bearer ${this.state.access_token}`,
                'Content-Type': 'application/json',
            },
            data
        });
    };

    getParams = (url) => {
        let queryStart = url.indexOf("?") + 1,
            queryEnd   = url.indexOf("#") + 1 || url.length + 1,
            query = url.slice(queryStart, queryEnd - 1),
            pairs = query.replace(/\+/g, " ").split("&"),
            parms = {}, i, n, v, nv;

        if (query === url || query === "") return;

        for (i = 0; i < pairs.length; i++) {
            nv = pairs[i].split("=", 2);
            n = decodeURIComponent(nv[0]);
            v = decodeURIComponent(nv[1]);

            if (!parms.hasOwnProperty(n)) parms[n] = [];
            parms[n].push(nv.length === 2 ? v : null);
        }
        return parms;
    };

    _onNavigationStateChange = (webViewState) => {

        // console.log('webViewState---->>', webViewState);

        if (webViewState.url.includes('https://example.com/')) {

            this.setState({
                approvalUrl: null
            });

            let params = this.getParams(webViewState.url);

            this.paymemtExecute(params.paymentId[0], params.PayerID[0]).then((resp) => {
                this._removeCartData(resp.data.transactions);

            }).catch((error) => {
                console.log(error);
            });

        }
    };

    _removeCartData = (payPalResponse) => {
        let cart = this.instance.getUserCart();
        let user = this.instance.getUser();
        Service._getOrderId((resp) => {
            if(!resp.error) {
                let orderID = resp.data.orderId + 1;
                Service._setUserOrder(orderID,user, cart,payPalResponse, (resp) => {
                    if(!resp.error) {
                        this.updateOrderId(orderID);
                    } else {
                        this.props.navigation.goBack();
                    }
                })
            }
        });
    };

    updateOrderId = (orderId) => {
        Service._updateOrderId(orderId, (resp) => {
            if(!resp.error) {
                this._removeCart()
            }
        });
    };

    _removeCart = () => {
        let array = [];
        Service._removeCart((resp) => {
            if(!resp.error) {
                this.instance.setUserCart(array);
                this.props.navigation.navigate('MyCartItem',{myCartItem: [], marketName: this.state.marketName, message: 'pago detectado con éxito'});
            }
        });
    };

    render() {
        const { approvalUrl, isLoading } = this.state;
        return (
            <View style={{flex: 1}}>
                {
                    approvalUrl ? <WebView
                            source={{ uri: approvalUrl }}
                            onNavigationStateChange={this._onNavigationStateChange}
                            javaScriptEnabled={true}
                            domStorageEnabled={true}
                            startInLoadingState={false}
                        /> :
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                            <ActivityIndicator />
                            <Text style={{fontSize: 20,
                                margin: 15,
                            }}>no presione el botón de retroceso</Text>
                        </View>
                }
            </View>
        );
    }
}
