import React from 'react';
import {
    ScrollView,
    Text,
    View,
    TouchableOpacity,
    Image,
    TextInput,
} from 'react-native';
import ProductList from './../../components/ProductList';
import CommonDataManager from '../../service/Singlenton';
import {hp, wp} from './../../UtilMethods/Utils';
import service from '../../firebase/Firebase';

export default class VerTodoList extends React.Component {
    instance = CommonDataManager.getInstance();

    constructor(props) {
        super(props);

        this.state = {
            products: this.props.navigation.state.params.product,
            catName: this.props.navigation.state.params.catName,
            productsArr: this.props.navigation.state.params.product,
            productName: this.props.navigation.state.params.productName,
            productsStr: '',
        }
    }

    _onPress = (item) => {
      // console.log('yyyyyy', item)
    };

    _handleSearchInput = (e) => {
        let text = e.toLowerCase().replace(/[^a-zA-Z ]/g, "");
        let fullList = this.state.productsArr;
        let filteredList = fullList.filter((item) => {
            if(item.name.toLowerCase().match(text)) {
                return item;
            }
        });
        if (!text || text === '') {
            this.setState({
                products: fullList,
                productsStr: ''
            });
        } else if (!filteredList.length) {
            this.setState({
                products: filteredList,
                productsStr: text
            });
        } else if (Array.isArray(filteredList)) {
            this.setState({
                products: filteredList,
                productsStr: text,
            });
        }
    };

    render() {
        return(
            <View style={{flex: 1,backgroundColor: '#f2efe9'}}>
            <View style={{alignItems: 'center',flexDirection: 'row', marginTop: hp('3%'), justifyContent: 'space-between'}}>
                <TouchableOpacity
                    onPress={() => this.props.navigation.goBack()}
                >
                    <Image source={require('./../../assets/icon/ic_back.png')}
                           style={{width: wp('6%'), height: wp('6%')}}
                    />
                </TouchableOpacity>
                <View>
                    <Image source={require('./../../assets/images/logo.png')}
                           style={{marginRight: wp('5%'),width: wp('12%'), height: wp('12%')}}
                    />
                </View>
                <View />
            </View>
            <View style={{borderWidth: .7}}/>
            <View style={{borderBottomWidth: 1, height: hp('7.4%'),
                marginLeft: wp('10%'), marginRight: wp('10%')}}>
                <View style={{flex: 1, flexDirection: 'row', marginTop: wp('3%')}}>
                    <Image source={require('./../../assets/icon/ic_search.png')}
                           style={{width: wp('7%'), height: wp('7%')}}
                    />
                    <View style={{flex: 1, marginLeft: 5}}>
                        <TextInput
                            placeholder={'Buscar Productos'}
                            autoCorrect={false}
                            value={this.state.productsStr}
                            onChangeText={productsStr => this._handleSearchInput(productsStr)}
                        />
                    </View>
                </View>
            </View>
            <ScrollView>
                <View style={{flex: 1}}>
                    {this.state.products.length > 0 && (
                        <ProductList color={'#f2efe9'}
                                     userCartList={this.instance.getUserCart()}
                                     onVerTodo={this._onPress.bind(this)}
                                     addToCart={this._addToCart.bind(this)}
                                     decrementProduct={this._decrementToCart.bind(this)}
                                     onCardPress={this._onCardPress.bind(this)}
                                     shop={this.state.productName}
                                     catName={this.state.catName}
                                     productList={this.state.products} />
                    )}
                </View>
            </ScrollView>
        </View>
        );
    }

    _addToCart = async (count, item) => {
        let userCart = this.instance.getUserCart();
        let arr = [];let newArray = [];
        if(userCart.length > 0) {
            userCart.map(async (cart, index) => {
                if(item.name === cart.name && item.category_type === cart.category_type) {
                    if(this.state.productName.toString() === cart.shop) {
                        item.quanity = cart.quanity + 1;
                        arr.push(item);
                        userCart.splice(index,1);
                    }
                }
            });
            if(arr.length === 0) {
                item.quanity = count;
                arr.push(item);
            }
            item.shop = this.state.productName;
            newArray = [...arr, ...userCart];
            this.instance.setUserCart(newArray);
            await service._addToCart('123',newArray, (resp) => {
                if(!resp.error) {
                    let cartCount = 0;
                    newArray.forEach((value) =>{
                        cartCount += parseInt(value.quanity);
                    });
                }
            });
        } else {
            this.onFirstAdd(item,arr,count).done();
        }
    };

    onFirstAdd = async (item, arr, count) => {
        item.quanity = count;
        item.shop = this.state.productName;
        arr.push(item);
        this.instance.setUserCart(arr);
        await service._addToCart('123',arr, (resp) => {
            if(!resp.error) {
                console.log('SUCCESS---->>>');
            }
        });
    };

    _decrementToCart = async (count, item) => {
        let arr = [];let newArray = [];
        let userCart = this.instance.getUserCart();

        if(count > 0) {
            userCart.map(async (cart, index) => {
                if(item.name === cart.name && item.category_type === cart.category_type && cart.shop === this.state.productName) {
                    item.quanity = cart.quanity - 1;
                    arr.push(item);
                    userCart.splice(index,1);
                }
            });
            item.shop = this.state.productName;
            newArray = [...arr, ...userCart];
            this.instance.setUserCart(newArray);
            await service._addToCart('123',newArray, (resp) => {
                console.log('Resp-Decrement---->>>>', resp);
                if(!resp.error) {
                    let cartCount = 0;
                    newArray.forEach((value) =>{
                        cartCount += parseInt(value.quanity);
                    });
                }
            });
        } else {
            userCart.map(async (cart, index) => {
                if(item.name === cart.name && item.category_type === cart.category_type) {
                    userCart.splice(index,1);
                }
            });
            item.shop = this.state.productName;
            newArray = [...userCart];
            this.instance.setUserCart(newArray);
            await service._addToCart('123',newArray, (resp) => {
                console.log('Resp-Decrement---->>>>', resp);
                if(!resp.error) {
                    let cartCount = 0;
                    newArray.forEach((value) =>{
                        cartCount += parseInt(value.quanity);
                    });
                }
            });
        }
    };

    _onCardPress = () => {
        //
    }

}
