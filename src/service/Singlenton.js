export default class CommonDataManager {

    static myInstance = null;

    _user = "";
    _userCart = "";

    /**
     * @returns {CommonDataManager}
     */
    static getInstance() {
        if (CommonDataManager.myInstance == null) {
            CommonDataManager.myInstance = new CommonDataManager();
        }

        return this.myInstance;
    }

    // Getter Setter of USER

    getUser() {
        return this._user;
    }

    setUser(res) {
        this._user = res;
    }

    // Getter Setter of COLLECTION

    getUserCart() {
        return this._userCart;
    }

    setUserCart(res) {
        this._userCart = res;
    }
}
