

const Styles = {
    gothamRounded_bold: {
        fontFamily: 'GOTHAMROUNDED-BOLD'
    },
    gothamRounded_light: {
        fontFamily: 'GOTHAMROUNDED-LIGHT'
    },
    gothamRounded_book: {
        fontFamily: 'GOTHAMROUNDEDBOOK'
    }
};

export default Styles;